package com.ufsc.enc.tcc.server.database;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.util.Constants;
import com.ufsc.enc.tcc.server.util.DataBases;
import com.ufsc.enc.tcc.server.util.GeneralOperations;
import com.ufsc.enc.tcc.server.util.JSONOperations;

public class InteractionBasis {

	private MongoDB mongoDB;

	public InteractionBasis(MongoDB mongoDB) {
		this.mongoDB = mongoDB;
	}

	public void insertInteraction(JSONObject interaction) {
		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());
		mongoDB.insert(JSONOperations.jsonObjectToDocument(interaction), DataBases.INTERACTION_BASE_NAME);
		// mongoDB.closeConnection();
	}

	public List<JSONObject> getInteractionsFridge(Integer idFridge) {
		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		List<JSONObject> jsonObjects = JSONOperations.documentListToJSONObjectList(
				mongoDB.search(Constants.ID_FRIDGE_KEY, idFridge, DataBases.INTERACTION_BASE_NAME));

		// mongoDB.closeConnection();

		return jsonObjects;
	}

	public List<JSONObject> getInteractionsFridge(Integer idFridge, Timestamp minimumTimestamp) {
		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		List<JSONObject> tmpList = JSONOperations.documentListToJSONObjectList(
				mongoDB.search(Constants.ID_FRIDGE_KEY, idFridge, DataBases.INTERACTION_BASE_NAME));
		// mongoDB.closeConnection();
		List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
		for (JSONObject jsonObject : tmpList) {
			try {
				String timeStmp = jsonObject.getString(Constants.TIMESTAMP_KEY);

				Timestamp currentTimestamp = Timestamp.valueOf(timeStmp);

				if (currentTimestamp.after(minimumTimestamp)) {
					jsonObjects.add(jsonObject);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return jsonObjects;
	}

	public JSONObject getLastInteraction(Integer idFridge) {
		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		List<JSONObject> jsonObjects = JSONOperations.documentListToJSONObjectList(
				mongoDB.search(Constants.ID_FRIDGE_KEY, idFridge, DataBases.INTERACTION_BASE_NAME));
		// mongoDB.closeConnection();

		if (jsonObjects == null || jsonObjects.isEmpty()) {
			return null;
		}

		System.out.println("GET_LAST_INT:\t" + jsonObjects);
		JSONObject json = GeneralOperations.getCurrentproductsByInteractions(jsonObjects, this.mongoDB);
		System.out.println(json);
		return jsonObjects.get(jsonObjects.size() - 1);
	}

	/**
	 * Remoção de todas os registros da coleção
	 * 
	 * @return
	 */
	public String removeAllInteractions() {
		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());
		mongoDB.removeAll(DataBases.INTERACTION_BASE_NAME);
		// mongoDB.closeConnection();

		return Constants.OK_JSON;
	}

	public List<JSONObject> listAllInteractions() {
		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());
		List<Document> tmpList = mongoDB.listEntries(DataBases.INTERACTION_BASE_NAME);
		// mongoDB.closeConnection();

		return JSONOperations.documentListToJSONObjectList(tmpList);
	}
}
