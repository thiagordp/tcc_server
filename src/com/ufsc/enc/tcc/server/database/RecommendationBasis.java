/**
 * 
 */
package com.ufsc.enc.tcc.server.database;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.util.Constants;
import com.ufsc.enc.tcc.server.util.DataBases;
import com.ufsc.enc.tcc.server.util.JSONOperations;

/**
 * @author trdp
 *
 */
public class RecommendationBasis
{

	private MongoDB mongoDB;

	public RecommendationBasis(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	/**
	 * É registrado um conjunto de produtos como recomendação que são mostrados aos poucos para o usuário.
	 * Cada produto é um obj.
	 * @param jsonObject
	 */
	public void insertProductRecommendation(Integer idFridge, List<JSONObject> jsonObjects, String typeRecommendation)
	{
		JSONObject json = new JSONObject();
		JSONArray jsonArray = new JSONArray();

		try {
			json.put(Constants.ID_FRIDGE_KEY, idFridge);

			Date date = new Date();
			Timestamp timestamp = new Timestamp(date.getTime());
			json.put(Constants.TIMESTAMP_KEY, timestamp.toString());
			json.put(Constants.RECOMMENDATION_TYPE_KEY, typeRecommendation);
			for (JSONObject jObject : jsonObjects) {
				jsonArray.put(jObject);
			}

			json.put(Constants.PRODUCT_LIST_KEY, jsonArray);
			Document temp = JSONOperations.jsonObjectToDocument(json);
			mongoDB.insert(temp, DataBases.RECOMMENDATION_BASE_NAME);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Seleciona o número de produtos, indicado, contidos na base de recomendações.
	 * @param idFridge Identificação da geladeira
	 * @param typeRecommendation Tipo de recomendação
	 * @param productCount Número de Itens recomendados
	 * @return List com os produtos recomendados
	 */
	public List<JSONObject> getProductRecommendation(Integer idFridge, String typeRecommendation, Integer productCount)
	{

		List<JSONObject> list = new ArrayList<>();															// Lista Resultante

		//MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put(Constants.ID_FRIDGE_KEY, idFridge);
			jsonObject.put(Constants.RECOMMENDATION_TYPE_KEY, typeRecommendation);

			Document doc = JSONOperations.jsonObjectToDocument(jsonObject);

			List<JSONObject> lstTmp = JSONOperations
					.documentListToJSONObjectList(mongoDB.search(doc, DataBases.RECOMMENDATION_BASE_NAME));		// Seleciona todas as recomendações para o usuário

			//	mongoDB.closeConnection();

			for (JSONObject json : lstTmp) {
				System.out.println(json);
				try {
					JSONArray jsonArray = json.getJSONArray(Constants.PRODUCT_LIST_KEY);						// Resgata o conjunto de produtos recomendados

					for (int i = 0; i < jsonArray.length(); i++) {												// Para cada produto, adiciona na lista final
						list.add(jsonArray.getJSONObject(i));

						// TODO Remover duplicatas

						if (list.size() == productCount) {														// Verifica se já foram resgatados todos os produtos necessário
							return list;
						}
					}

				}
				catch (JSONException e) {
					e.printStackTrace();
				}
				if (list.size() == productCount) {
					break;
				}
			}
		}
		catch (JSONException e1) {
			e1.printStackTrace();
		}

		// TODO: Remover aqueles que já foram selecionados
		return list;

	}

	/**
	 * É registrado um conjunto de receitas como recomendação que são mostradas aos poucos para o usuário.
	 * Cada receita é um jobj.
	 * @param jsonObject
	 */
	public void insertRecipeRecommendation(Integer idFridge, List<JSONObject> jsonObjects)
	{

		JSONObject json = new JSONObject();
		JSONArray jsonArray = new JSONArray();

		try {
			json.put(Constants.ID_FRIDGE_KEY, idFridge);
			json.put(Constants.RECOMMENDATION_TYPE_KEY, Constants.RECIPE_REC);

			Date date = new Date();
			Timestamp timestamp = new Timestamp(date.getTime());
			json.put(Constants.TIMESTAMP_KEY, timestamp.toString());

			for (JSONObject jObject : jsonObjects) {
				jsonArray.put(jObject);
			}

			json.put(Constants.PRODUCT_LIST_KEY, jsonArray);

		}
		catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public List<JSONObject> getRecipeRecommendation(Integer idFridge, Integer countRecipe)
	{

		List<JSONObject> list = new ArrayList<>();															// Lista Resultante

		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put(Constants.ID_FRIDGE_KEY, idFridge);
			jsonObject.put(Constants.RECOMMENDATION_TYPE_KEY, Constants.RECIPE_REC);

			Document doc = JSONOperations.jsonObjectToDocument(jsonObject);

			List<JSONObject> lstTmp = JSONOperations
					.documentListToJSONObjectList(mongoDB.search(doc, DataBases.RECOMMENDATION_BASE_NAME));		// Seleciona todas as recomendações para o usuário

			System.out.println("DB: " + lstTmp);

			for (JSONObject json : lstTmp) {
				System.out.println(json);

				try {
					JSONArray jsonArray = json.getJSONArray(Constants.PRODUCT_LIST_KEY);						// Resgata o conjunto de produtos recomendados

					for (int i = 0; i < jsonArray.length(); i++) {												// Para cada produto, adiciona na lista final
						list.add(jsonArray.getJSONObject(i));

						// TODO Remover duplicatas

						if (list.size() == countRecipe) {														// Verifica se já foram resgatados todos os produtos necessário
							return list;
						}
					}

				}
				catch (JSONException e) {
					e.printStackTrace();
				}
				if (list.size() == countRecipe) {
					break;
				}
			}
		}
		catch (JSONException e1) {
			e1.printStackTrace();
		}

		// TODO: Remover aqueles que já foram selecionados
		return list;

	}

	public List<JSONObject> getAllRecommendation()
	{
		return JSONOperations
				.documentListToJSONObjectList(this.mongoDB.listEntries(DataBases.RECOMMENDATION_BASE_NAME));
	}

	public void removeAllRecommendation()
	{
		//	MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		mongoDB.removeAll(DataBases.RECOMMENDATION_BASE_NAME);
		//	mongoDB.closeConnection();
	}
}
