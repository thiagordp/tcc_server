/**
 * 
 */
package com.ufsc.enc.tcc.server.database;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.util.Constants;
import com.ufsc.enc.tcc.server.util.DataBases;
import com.ufsc.enc.tcc.server.util.JSONOperations;

/**
 * @author trdp
 *
 */
public class MetaInformationBasis
{

	private MongoDB mongoDB;

	public MetaInformationBasis(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	/**
	 * Resgata as meta-informações do produto indicado
	 * @param idProduct
	 * @return
	 */
	public JSONObject getProductMetaInfo(String idProduct)
	{
		// TODO Testar todas as possibilidades

		//MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());
		JSONObject json = new JSONObject();
		List<JSONObject> jsonObjects = JSONOperations.documentListToJSONObjectList(
				mongoDB.search(Constants.PRODUCT_BARCODE_KEY, idProduct, DataBases.META_INFO_BASE_NAME));
		//	mongoDB.closeConnection();

		if (jsonObjects.isEmpty()) {
			return json;
		}

		return jsonObjects.get(0);
	}

	/**
	 * 
	 * @param idProducts
	 * @return Nothing
	 */
	public List<JSONObject> getProductsMetaInfo(List<String> idProducts)
	{

		// TODO Testar
		List<JSONObject> list = new ArrayList<>();

		for (String idProduct : idProducts) {
			JSONObject json = this.getProductMetaInfo(idProduct);

			if (json.has(Constants.PRODUCT_BARCODE_KEY)) {
				list.add(json);
			}
		}

		return list;
	}

	/**
	 * Resgata as meta-informações do produto indicado
	 * @param idProduct
	 * @return
	 */
	public JSONObject getProductMetaInfoByEPC(Long manufacturer, Long product)
	{

		//JSONObject json = new JSONObject();
		//MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		JSONObject jsonFilter = new JSONObject();
		JSONObject jsonFilterIntern = new JSONObject();

		try {
			jsonFilterIntern.put(Constants.EPC_MANUFACTURER_KEY, manufacturer);
			jsonFilterIntern.put(Constants.EPC_PRODUCT_KEY, product);
			jsonFilter.put(Constants.EPC_KEY, jsonFilterIntern);

			List<JSONObject> jsonObjects = JSONOperations.documentListToJSONObjectList(
					mongoDB.search(JSONOperations.jsonObjectToDocument(jsonFilter), DataBases.META_INFO_BASE_NAME));
			//	mongoDB.closeConnection();

			if (jsonObjects.isEmpty()) {
				return null;
			}

			return jsonObjects.get(0);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 * @param product
	 */
	public String updateProductMetaInfo(JSONObject product)
	{
		// TODO Testar
		//MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());
		String json = Constants.OK_JSON;

		try {

			Integer idProduct = product.getInt(Constants.PRODUCT_BARCODE_KEY);

			List<JSONObject> listSearch = JSONOperations.documentListToJSONObjectList(
					mongoDB.search(Constants.PRODUCT_BARCODE_KEY, idProduct, DataBases.META_INFO_BASE_NAME));

			if (listSearch.isEmpty()) {
				mongoDB.insert(JSONOperations.jsonObjectToDocument(product), DataBases.META_INFO_BASE_NAME);
			}
			else {
				mongoDB.remove(JSONOperations.jsonObjectToDocument(listSearch.get(0)), DataBases.META_INFO_BASE_NAME);
				mongoDB.insert(JSONOperations.jsonObjectToDocument(product), DataBases.META_INFO_BASE_NAME);
			}

		}
		catch (JSONException e) {
			e.printStackTrace();
			json = Constants.ERROR_JSON;
		}

		//mongoDB.closeConnection();

		return json;
	}

	/**
	 * 
	 * @param productList
	 */
	public void updateProductsMetaInfo(List<JSONObject> productList)
	{

		// TODO Testar
		//	MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		for (JSONObject jsonObject : productList) {
			try {
				Integer idProduct = jsonObject.getInt(Constants.PRODUCT_BARCODE_KEY);

				List<JSONObject> listSearch = JSONOperations.documentListToJSONObjectList(
						mongoDB.search(Constants.PRODUCT_BARCODE_KEY, idProduct, DataBases.META_INFO_BASE_NAME));

				if (listSearch.isEmpty()) {
					mongoDB.insert(JSONOperations.jsonObjectToDocument(jsonObject), DataBases.META_INFO_BASE_NAME);
				}
				else {
					mongoDB.remove(JSONOperations.jsonObjectToDocument(listSearch.get(0)),
							DataBases.META_INFO_BASE_NAME);
					mongoDB.insert(JSONOperations.jsonObjectToDocument(jsonObject), DataBases.META_INFO_BASE_NAME);
				}
			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		//	mongoDB.closeConnection();
	}

	public List<JSONObject> getAllProductMetaInfo()
	{
		//	MongoDB mongoDB = new MongoDB(Dat
		List<Document> tmpList = mongoDB.search(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY,
				DataBases.META_INFO_BASE_NAME);

		return JSONOperations.documentListToJSONObjectList(tmpList);
	}

	public List<JSONObject> getAllRecipeMetaInfo()
	{
		//MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());
		List<Document> tmpList = mongoDB.search(Constants.META_INFO_TYPE_KEY, Constants.META_INFO_RECIPE,
				DataBases.META_INFO_BASE_NAME);
		//List<Document> tmpList = mongoDB.listEntries(DataBases.META_INFO_BASE_NAME);
		return JSONOperations.documentListToJSONObjectList(tmpList);
	}

	public void removeAllMetainformation()
	{
		//	MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());
		mongoDB.removeAll(DataBases.META_INFO_BASE_NAME);
		//	mongoDB.closeConnection();
	}
}
