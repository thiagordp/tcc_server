/**
 * 
 */
package com.ufsc.enc.tcc.server.database;

import java.util.List;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.util.Constants;
import com.ufsc.enc.tcc.server.util.DataBases;
import com.ufsc.enc.tcc.server.util.GeneralOperations;
import com.ufsc.enc.tcc.server.util.JSONOperations;

/**
 * @author trdp
 *
 */
public class AuxiliaryStructureBasis {

	private MongoDB mongoDB;

	public AuxiliaryStructureBasis(MongoDB mongoDB) {
		this.mongoDB = mongoDB;
	}

	/**
	 * 
	 * @param id_fridge
	 * @param status
	 */
	public void setDoorStatus(Integer id_fridge, Integer status) {
		JSONObject jsonObject = new JSONObject();
		// MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME,
		// DataBases.getCollections());
		try {

			jsonObject.put(Constants.ID_FRIDGE_KEY, id_fridge);
			jsonObject.put(Constants.RECORD_TYPE_KEY, Constants.RECORD_TYPE_OPEN_DOOR_KEY);
			jsonObject.put(Constants.DOOR_STATUS_KEY, status);

			mongoDB.insert(JSONOperations.jsonObjectToDocument(jsonObject), DataBases.AUX_STRUCT_BASE_NAME);

		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			// mongoDB.closeConnection();
		}
	}

	/**
	 * 
	 * @param id_fridge
	 * @return
	 */
	public JSONObject getDoorStatus(Integer id_fridge) {

		// MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME,
		// DataBases.getCollections());
		JSONObject json = null;

		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(Constants.ID_FRIDGE_KEY, id_fridge);
			jsonObject.put(Constants.RECORD_TYPE_KEY, Constants.RECORD_TYPE_OPEN_DOOR_KEY);

			List<JSONObject> list = JSONOperations.documentListToJSONObjectList(
					mongoDB.search(JSONOperations.jsonObjectToDocument(jsonObject), DataBases.AUX_STRUCT_BASE_NAME));

			if (!list.isEmpty()) {
				json = list.get(list.size() - 1);
			} else {
				json = GeneralOperations.JSONMessage("N�o h� registros de porta aberta");
			}

		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			// mongoDB.closeConnection();
		}

		return json;
	}

	/**
	 * Edita as configurações do Aplicativo
	 * 
	 * @param id_fridge
	 * @param settings
	 */
	public void editSettings(JSONObject settings) {

		// MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME,
		// DataBases.getCollections());

		mongoDB.insert(JSONOperations.jsonObjectToDocument(settings), DataBases.AUX_STRUCT_BASE_NAME);
		/// mongoDB.closeConnection();
	}

	/**
	 * 
	 * @param id_fridge
	 * 			@throws
	 */
	public JSONObject getSettings(Integer id_fridge) {

		// MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME,
		// DataBases.getCollections());

		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(Constants.ID_FRIDGE_KEY, id_fridge);
			jsonObject.put(Constants.RECORD_TYPE_KEY, Constants.RECORD_TYPE_SETTINGS_KEY);

			List<JSONObject> list = JSONOperations.documentListToJSONObjectList(
					mongoDB.search(JSONOperations.jsonObjectToDocument(jsonObject), DataBases.AUX_STRUCT_BASE_NAME));

			// mongoDB.closeConnection();
			if (!list.isEmpty()) {
				jsonObject = list.get(list.size() - 1);
			} else {
				jsonObject = GeneralOperations.JSONMessage("Não há registros de configurações");
			}

			return jsonObject;
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			// mongoDB.closeConnection();
		}

		return null;
	}

	public String insertPurchaseList(JSONObject jsonList) {

		// MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME,
		// DataBases.getCollections());
		String result = "";
		try {
			jsonList.put(Constants.RECORD_TYPE_KEY, Constants.TYPE_PURCHASE_LIST_KEY);

			mongoDB.insert(JSONOperations.jsonObjectToDocument(jsonList), DataBases.AUX_STRUCT_BASE_NAME);
			result = Constants.OK_JSON;
		} catch (JSONException e) {
			e.printStackTrace();
			result = Constants.ERROR_JSON;
		} finally {
			// mongoDB.closeConnection();
		}

		return result;
	}

	/**
	 * Ao mesmo tempo que lê já deve apagar
	 * 
	 * @param idFridge
	 * @return
	 */
	public List<JSONObject> getPendentPurchaseLists(Integer idFridge) {

		// MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME,
		// DataBases.getCollections());
		JSONObject jsonList = new JSONObject();
		List<JSONObject> list = null;

		try {
			jsonList.put(Constants.ID_FRIDGE_KEY, idFridge);
			jsonList.put(Constants.RECORD_TYPE_KEY, Constants.TYPE_PURCHASE_LIST_KEY);

			List<Document> listDoc = mongoDB.search(JSONOperations.jsonObjectToDocument(jsonList),
					DataBases.AUX_STRUCT_BASE_NAME);
			list = JSONOperations.documentListToJSONObjectList(listDoc);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// mongoDB.closeConnection();

		return list;
	}

	// TODO remoção da lista de produtos
	public void removePurchaseList(Integer idFridge) {

	}

	public void removeAllAuxiliary() {

		// MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME,
		// DataBases.getCollections());
		mongoDB.removeAll(DataBases.AUX_STRUCT_BASE_NAME);
		// mongoDB.closeConnection();

	}
}
