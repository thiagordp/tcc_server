/**
 * 
 */
package com.ufsc.enc.tcc.server.model;

import java.math.BigInteger;

import com.ufsc.enc.tcc.server.util.Constants;

/**
 * @author trdp
 *
 */
public class EPC
{

	/**
	 * 
	 */
	private BigInteger epcCode;

	/**
	 * 
	 * @param epcCode
	 */
	public EPC(BigInteger epcCode)
	{
		this.epcCode = epcCode;
	}

	/**
	 * 
	 * @return
	 */
	public Long getHeader()
	{
		String strValue = epcCode.shiftRight(Constants.EPC_BIT_HEADER).toString();

		return Long.valueOf(strValue);
	}

	/**
	 * 
	 * @return
	 */
	public Long getEPCManagerNumber()
	{
		// Desloca para direita eliminar dígitos à direita
		BigInteger tmpValue = epcCode.shiftRight(Constants.EPC_BIT_MANUFACTURER);

		// Faz operação bit a bit de AND para eliminar dígitos à esquerda.
		tmpValue = tmpValue.and(new BigInteger(Constants.EPC_BIT_MASK_MANUFACTURER));

		return Long.valueOf(tmpValue.toString());
	}

	/**
	 * 
	 * @return
	 */
	public Long getObjectClass()
	{
		// Desloca para direita eliminar dígitos à direita
		//	System.out.println("EPC:\t " + this.epcCode.toString());
		BigInteger tmpValue = epcCode.shiftRight(Constants.EPC_BIT_PRODUCT);
		//	System.out.println("AFTERSHIFT\t" + tmpValue.toString());
		// Faz operação bit a bit de AND para eliminar dígitos à esquerda.
		tmpValue = tmpValue.and(new BigInteger(Constants.EPC_BIT_MASK_PRODUCT));
		//	System.out.println("AFTER AND\t" + tmpValue.toString());
		return Long.valueOf(tmpValue.toString());
	}

	/**
	 * 
	 * @return
	 */
	public Long getSerialNumber()
	{

		// Faz operação bit a bit de AND para eliminar dígitos à esquerda.
		BigInteger tmpValue = epcCode.and(new BigInteger(Constants.EPC_BIT_MASK_SERIAL_NUMBER));

		return Long.valueOf(tmpValue.toString());
	}
}
