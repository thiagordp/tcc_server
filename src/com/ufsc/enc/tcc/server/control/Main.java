/**
 * 
 */
package com.ufsc.enc.tcc.server.control;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

/**
 * @author trdp
 *
 */
public class Main extends Application {
	public Set<Class<?>> getClasses() {
		Set<Class<?>> s = new HashSet<Class<?>>();
		s.add(ServicesServer.class);

		return s;
	}
}
