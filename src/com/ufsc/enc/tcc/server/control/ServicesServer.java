/**
 * 
 */
package com.ufsc.enc.tcc.server.control;

import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.database.AuxiliaryStructureBasis;
import com.ufsc.enc.tcc.server.database.MetaInformationBasis;
import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.database.RecommendationBasis;
import com.ufsc.enc.tcc.server.service.AvailableProductQuery;
import com.ufsc.enc.tcc.server.service.ExpiredProductQuery;
import com.ufsc.enc.tcc.server.service.FillDatabase;
import com.ufsc.enc.tcc.server.service.FillMetaInfo;
import com.ufsc.enc.tcc.server.service.FillRecipe;
import com.ufsc.enc.tcc.server.service.InteractionRecord;
import com.ufsc.enc.tcc.server.service.OpenDoorQuery;
import com.ufsc.enc.tcc.server.service.PurchaseRecommenderQuery;
import com.ufsc.enc.tcc.server.service.RecipeRecommendQuery;
import com.ufsc.enc.tcc.server.service.Settings;
import com.ufsc.enc.tcc.server.util.Constants;
import com.ufsc.enc.tcc.server.util.DataBases;
import com.ufsc.enc.tcc.server.util.GeneralOperations;
import com.ufsc.enc.tcc.server.util.JSONOperations;

/**
 * @author trdp
 */
@Path("/")
public class ServicesServer implements ServletContextListener {
	private MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());;

	/******************************************************************/

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		this.mongoDB.closeConnection();

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("Boa noite");
	}

	/******************************************************************/

	@GET
	@Path("/generic")
	@Produces(MediaType.APPLICATION_JSON)
	public String generic(@QueryParam("head") Long head, @QueryParam("manufacturer") Long manufacturer,
			@QueryParam("product") Long product, @QueryParam("serial") Long serial) {
		// new AuxiliaryStructureBasis(mongoDB).setDoorStatus(1, Constants.DOOR_OPENED);
		this.mongoDB.removeAll(DataBases.RECOMMENDATION_BASE_NAME);

		AuxiliaryStructureBasis auxiliaryStructureBasis = new AuxiliaryStructureBasis(this.mongoDB);
		auxiliaryStructureBasis.setDoorStatus(2, Constants.DOOR_OPENED);

		return this.mongoDB.listEntries(DataBases.AUX_STRUCT_BASE_NAME).toString();
		// return "{}";

		// MetaInformationBasis metaInformationBasis = new
		// MetaInformationBasis(this.mongoDB);
		// return metaInformationBasis.getAllProductMetaInfo().toString();

		// auxiliaryStructureBasis.getDoorStatus(1).toString();
		// auxiliaryStructureBasis.getPendentPurchaseLists(1);

		// return auxiliaryStructureBasis.getSettings(1).toString();
		// RecommendationBasis reBasis = new RecommendationBasis(this.mongoDB);
		// return reBasis.getProductRecommendation(1, Constants.PRODUCT_REC_NEW,
		// 3).toString();
		// MetaInformationBasis basis = new MetaInformationBasis(this.mongoDB);
		// return basis.getAllRecipeMetaInfo().toString();
		/*
		 * RecommendationBasis recommendationBasis = new
		 * RecommendationBasis(this.mongoDB); return
		 * recommendationBasis.getProductRecommendation(1, Constants.PRODUCT_REC_NEW,
		 * 3).toString() + "\r\n\r\n\r\n" +
		 * recommendationBasis.getRecipeRecommendation(1, 2);
		 */

		/*
		 * MetaInformationBasis metaInformationBasis = new
		 * MetaInformationBasis(this.mongoDB);
		 * 
		 * List<JSONObject> list = metaInformationBasis.getAllProductMetaInfo();
		 * System.out.println("Count Prod: " + list.size());
		 * 
		 * list = metaInformationBasis.getAllRecipeMetaInfo();
		 * System.out.println("Count Reci:" + list.size());
		 * 
		 * return list.toString();
		 */

		// RecommendationBasis recommendationBasis = new
		// RecommendationBasis(this.mongoDB);
		// return recommendationBasis.getAllRecommendation().toString();

		// return GeneralOperations.generateEPC(head, manufacturer, product,
		// serial).toString();

		/*
		 * JSONObject interaction = new JSONObject(); JSONArray epcCodes = new
		 * JSONArray();
		 * 
		 * ////////////////////////////////////////////////////////// // OBTENÇÃO DO
		 * CÓDIGO EPC A PARTIR DOS DADOS DO PRODUTO //
		 * //////////////////////////////////////////////////////////
		 * MetaInformationBasis metaInformationBasis = new
		 * MetaInformationBasis(mongoDB);
		 * metaInformationBasis.removeAllMetainformation();
		 * 
		 * FillMetaInfo fillMetaInfo = new FillMetaInfo(mongoDB);
		 * fillMetaInfo.fillEntries(); FillRecipe fillRecipe = new FillRecipe(mongoDB);
		 * fillRecipe.fillEntries();
		 * 
		 * /* InteractionBasis interactionBasis = new InteractionBasis(mongoDB);
		 * interactionBasis.removeAllInteractions();
		 * 
		 * FillInteraction fillInteraction = new FillInteraction(mongoDB);
		 * fillInteraction.fillEntries();
		 * 
		 * 
		 * RecommendationBasis recommendationBasis = new RecommendationBasis(mongoDB);
		 * recommendationBasis.removeAllRecommendation();
		 * 
		 * return "{}";
		 */

		/*
		 * FillDatabase fillDatabase = new FillDatabase(mongoDB);
		 * fillDatabase.eraseAuxiliaryBase(); fillDatabase.fillAuxiliaryBase();
		 * 
		 * return "OK";
		 */
		/*
		 * BigInteger epc = new BigInteger("8665580291139087593275130006");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580291139087593275129857");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580291139087593275129869");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580291139087593275129875");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580291139087593275129890");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580288832880851871072257");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580288832880851871072350");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580288832880851871072400");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580288832880851871072406");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580287680110049076707478");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580287680110049076707330");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580287680110049076707331");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580287680110049076707390");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580287680110049076707432");
		 * epcCodes.put(epc.toString());
		 * 
		 * epcCodes.put(epc.toString());
		 * 
		 * try { interaction.put(Constants.EPC_CODES_KEY, epcCodes);
		 * interaction.put(Constants.ID_FRIDGE_KEY, 2);
		 * interaction.put(Constants.TIMESTAMP_KEY, GeneralOperations.getTimestamp());
		 * 
		 * } catch (JSONException e) { e.printStackTrace(); }
		 * 
		 * new InteractionBasis(mongoDB).insertInteraction(interaction); return
		 * interaction.toString();
		 */

		// Interação para teste com receitas

		/*
		 * MetaInformationBasis metaInformationBasis = new
		 * MetaInformationBasis(mongoDB);
		 * metaInformationBasis.removeAllMetainformation();
		 * 
		 * FillRecipe fillRecipe = new FillRecipe(mongoDB); fillRecipe.fillEntries();
		 * FillMetaInfo fillMetaInfo = new FillMetaInfo(mongoDB);
		 * fillMetaInfo.fillEntries(); /* InteractionBasis basis = new
		 * InteractionBasis(mongoDB); basis.removeAllInteractions();
		 * 
		 * BigInteger epc = new BigInteger("8665580302667778103577673729");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580302667778103577673800");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580280762279342932754437");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580280762279342932755932");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580288833156623131213860");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580288833156623131213874");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580288833156623131213883");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580288833156623131213902");
		 * epcCodes.put(epc.toString());
		 * 
		 * epc = new BigInteger("8665580288833156623131213863");
		 * epcCodes.put(epc.toString());
		 * 
		 * try { interaction.put(Constants.EPC_CODES_KEY, epcCodes);
		 * interaction.put(Constants.ID_FRIDGE_KEY, 2);
		 * interaction.put(Constants.TIMESTAMP_KEY, GeneralOperations.getTimestamp());
		 * 
		 * } catch (JSONException e) { e.printStackTrace(); }
		 * 
		 * new InteractionBasis(mongoDB).insertInteraction(interaction); return
		 * interaction.toString();
		 */
	}

	@GET
	@Path("/metainfo")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllMetaInfo() {
		JSONObject result = new JSONObject();
		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);
		List<JSONObject> list = metaInformationBasis.getAllProductMetaInfo();

		for (int i = 0; i < list.size(); i++) {
			try {
				result.put("Name" + String.valueOf(i), list.get(i).getString(Constants.PRODUCT_DESCRIPTION_KEY));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		FillRecipe fillRecipe = new FillRecipe(this.mongoDB);
		fillRecipe.fillEntries();

		return result.toString();
	}

	@POST
	@Path("/filldatabases")
	@Produces(MediaType.APPLICATION_JSON)
	public String fillDatabases(String data) {
		Long t1 = System.nanoTime();

		FillDatabase fillDatabase = new FillDatabase(this.mongoDB);

		// Apagar todos as bases
		fillDatabase.eraseInteractionBase();
		fillDatabase.eraseAuxiliaryBase();
		fillDatabase.eraseMetaInfoBase();
		fillDatabase.eraseRecommendationBase();

		// Preenchimento de todas as bases.
		fillDatabase.fillMetaInfoBase();
		fillDatabase.fillInteractionBase();
		fillDatabase.fillAuxiliaryBase();
		fillDatabase.fillRecommendationBase();

		FillRecipe fillRecipe = new FillRecipe(mongoDB);
		fillRecipe.fillEntries();

		// MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME,
		// DataBases.getCollections());
		// List<JSONObject> list = JSONOperations
		// .documentListToJSONObjectList(mongoDB.listEntries(DataBases.AUX_STRUCT_BASE_NAME));

		// mongoDB.closeConnection();

		Long t2 = System.nanoTime();

		GeneralOperations.showTime(t2 - t1);

		return "{\"status\":1}";
	}

	/************************************************************************************
	 * 
	 * Há duas possibilidades: registro de interação ou de porta aberta que são
	 * redirecionados para a base correta.
	 * 
	 * @return
	 ************************************************************************************/
	@POST
	@Path("/register")
	@Produces(MediaType.APPLICATION_JSON)
	public String register(@QueryParam("type_record") Integer typeRecord, String data) {
		System.out.println("POST: /register");

		System.out.println("\tdata:\t" + data);
		InteractionRecord record = new InteractionRecord(this.mongoDB);

		return record.recordNewInteraction(typeRecord, data);
	}

	/************************************************************************************
	 * Resgata os produtos contidos atualmente na geladeira
	 * 
	 * @param idFridge
	 *            Identificação da geladeira
	 * @return String JSON com lista de produtos contidos
	 ************************************************************************************/
	@GET
	@Path("/available_products")
	@Produces(MediaType.APPLICATION_JSON)
	public String availableProductsQuery(@QueryParam("id_fridge") Integer idFridge) {

		AvailableProductQuery aProductQuery = new AvailableProductQuery(this.mongoDB); // Classe do serviço de
																						// verificação e resgate de
																						// produtos

		return aProductQuery.getAvailableProducts(idFridge); // Resgata a lista de produtos referentes ao identificador
																// da geladeira informado.
	}

	/************************************************************************************
	 * Resgate de produtos vencidos ou que irão vencer dentro de um determinado
	 * número de dias
	 * 
	 * @param idFridge
	 *            Identificação da geladeira
	 * @param referenceDays
	 *            Número de dias de refências em relação ao dia atual em que
	 *            produtos estarão vencidos.
	 * @return JSON com lista de produtos que vencidos ou que ficarão vencidos.
	 ************************************************************************************/
	@GET
	@Path("/expired_products")
	public String expiredProducts(@QueryParam("id_fridge") Integer idFridge,
			@QueryParam("reference_days") Integer referenceDays) {
		ExpiredProductQuery eProductQuery = new ExpiredProductQuery(this.mongoDB); // Classe do serviço de
																					// verificação e resgate de
																					// produtos vencidos

		return eProductQuery.getExpiredProduct(idFridge, referenceDays); // Resgata a lista de produtos vencidos
																			// referentes ao identificador
																			// passado dentro do intervalo informado.
	}

	/************************************************************************************
	 * Verificação do status atual da porta da geladeira com identificador indicado.
	 * 
	 * @param idFridge
	 *            Identificação da geladeira
	 * @return JSON que informa o status atual da geladeira
	 ************************************************************************************/
	@GET
	@Path("/open_door")
	@Produces(MediaType.APPLICATION_JSON)
	public String openDoorQuery(@QueryParam("id_fridge") Integer idFridge) {
		System.out.println("POST: /open_door");
		System.out.println("\ttypeReg:\t" + idFridge);
		OpenDoorQuery oDoorQuery = new OpenDoorQuery(this.mongoDB); // Classe do serviço

		return oDoorQuery.getStatusDoor(idFridge); // Resgata o status da porta
	}

	/************************************************************************************
	 * @return
	 ************************************************************************************/
	@POST
	@Path("/settings")
	@Produces(MediaType.APPLICATION_JSON)
	public String settings(@QueryParam("action_type") Integer actionType, @QueryParam("id_fridge") Integer idFridge,
			String data) {

		Settings settings = new Settings(this.mongoDB);

		if (actionType == 1) { // Escrever configurações
			return settings.setSettings(idFridge, data);
		} else { // Ler configurações
			return settings.getSettings(idFridge, data);
		}
	}

	/************************************************************************************
	 * Recomenda compras de produtos
	 * 
	 * @param idFridge
	 *            Identificação da geladeira
	 * @return JSON com lista de produtos disponíveis para compra e que podem ser
	 *         úteis
	 ************************************************************************************/
	@GET
	@Path("/purchase_recommender")
	@Produces(MediaType.APPLICATION_JSON)
	public String purchaseRecommender(@QueryParam("id_fridge") Integer idFridge) {

		System.out.println("\n\nPurchase recommendation\n\n");
		// Classe que recomenda compra de produtos
		PurchaseRecommenderQuery purchaseRecommenderQuery = new PurchaseRecommenderQuery(this.mongoDB);

		// Retorna a lista de produtos recomendados
		return purchaseRecommenderQuery.getPurchaseRecommendation(idFridge);
	}

	/************************************************************************************
	 * Retorna lista de recomendações de receitas
	 * 
	 * @param idFridge
	 *            Identificador da geladeira
	 * @return JSON contendo a lista de receitas recomendadas
	 ************************************************************************************/
	@GET
	@Path("/recipe_recommender")
	@Produces(MediaType.APPLICATION_JSON)
	public String recipeRecommender(@QueryParam("id_fridge") Integer idFridge) {

		// Classe que recomenda receitas
		RecipeRecommendQuery receipeRecommendQuery = new RecipeRecommendQuery(this.mongoDB);

		// Resgate de recomendações de receita
		return receipeRecommendQuery.getRecipeRecommendation(idFridge);
	}

	/**
	 * 
	 * @param recipeName
	 * @param recipeUrl
	 * @return
	 */
	@POST
	@Path("/recipe_info")
	@Produces(MediaType.APPLICATION_JSON)
	public String recipeInfo(String data) {

		JSONObject json;
		try {
			json = new JSONObject(data);
			System.out.println(json);

			/*
			 * Document docFilter = new Document();
			 * docFilter.put(Constants.RECIPE_DESCRIPTION_KEY, "");
			 * docFilter.put(Constants.RECIPE_URL_KEY, "");
			 */

			System.out.println(this.mongoDB.listEntries(DataBases.META_INFO_BASE_NAME));

			List<Document> listDoc = this.mongoDB.search(JSONOperations.jsonObjectToDocument(json),
					DataBases.META_INFO_BASE_NAME);

			if (listDoc == null || listDoc.isEmpty()) {
				return "{}";
			}

			return listDoc.get(0).toJson();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return "{}";
	}

	@POST
	@Path("/purchase_list")
	public String purchaseList(String data) {

		System.out.println("PURCHASE_LIST:\t" + data);

		try {
			JSONObject json = new JSONObject(data);

			Integer userId = json.getInt(Constants.ID_FRIDGE_KEY);
			JSONArray products = json.getJSONArray(Constants.PRODUCT_LIST_KEY);

			if (products == null || products.length() == 0) {
				return Constants.ERROR_JSON;
			}

			json.put(Constants.RECORD_TYPE_KEY, Constants.RECORD_TYPE_PURCHASE_LIST);
			Document document = JSONOperations.jsonObjectToDocument(json);

			this.mongoDB.insert(document, DataBases.AUX_STRUCT_BASE_NAME);
			System.out.println("JSON_PURCHASE_LIST:\t" + document.toJson());

			return Constants.OK_JSON;

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return "{}";
	}
}