package com.ufsc.enc.tcc.server.util;

public class MetaInfoConstants
{
	//#####################   PRODUCTS   #####################################//

	/**
	 * Classes
	 */
	public static final Integer AGUA = 16;
	public static final Integer LATICINIO = 1;
	public static final Integer EMBUTIDO = 2;
	public static final Integer BEBIDA = 3;
	public static final Integer CONGELADO = 4;
	public static final Integer CARNE = 5;
	public static final Integer CONFEITARIA = 6;
	public static final Integer GRANJA = 7;
	public static final Integer MERCEARIA = 8;
	public static final Integer FARINHAS = 9;
	public static final Integer FEIRA = 10;
	public static final Integer SAL_PIMENTA = 11;
	public static final Integer PADARIA = 12;
	public static final Integer TEMPEROS = 13;
	public static final Integer MOLHOS = 14;
	public static final Integer LEGUMES = 15;

	/**
	 * Produtos
	 */
	public static final Integer AGUA_QUENTE = 36;
	public static final Integer LEITE = 1;
	public static final Integer MARGARINA = 2;
	public static final Integer LEITE_CONDENSADO = 3;
	public static final Integer CREME_DE_LEITE = 4;
	public static final Integer DOCE = 5;
	public static final Integer IOGURTE = 6;
	public static final Integer QUEIJO_MUSSARELA = 7;
	public static final Integer MORTADELA = 8;
	public static final Integer CERVEJA = 9;
	public static final Integer REFRIGERANTE = 10;
	public static final Integer PIZZA = 11;
	public static final Integer LINGUICA = 12;
	public static final Integer ISCAS_CARNE = 13;
	public static final Integer BIFE = 14;
	public static final Integer MAMINHA = 15;
	public static final Integer LASANHA = 16;
	public static final Integer SALSICHA = 17;
	public static final Integer FERMENTO = 18;
	public static final Integer ACUCAR = 19;
	public static final Integer OVO = 19;
	public static final Integer OLEO = 20;
	public static final Integer FARINHA_TRIGO = 21;
	public static final Integer FARINHA_ROSCA = 22;
	public static final Integer ALHO = 23;
	public static final Integer SAL = 24;
	public static final Integer PIMENTA = 25;
	public static final Integer PAO = 26;
	public static final Integer MANJERICAO = 27;
	public static final Integer COXAO_MOLE = 28;
	public static final Integer CEBOLA = 29;
	public static final Integer UMAM = 30;
	public static final Integer SALSINHA = 31;
	public static final Integer EXTRATO_TOMATE = 32;
	public static final Integer TOMATE = 33;
	public static final Integer CENOURA = 34;
	public static final Integer OREGANO = 35;

	//#####################   RECIPES   #####################################//

	/***************************************************
	 * Classes
	 ***************************************************/

	public static final Integer CAKE_PIE = 1;
	public static final Integer MEAT = 2;
	public static final Integer CANDY_DESSERT = 3;
	public static final Integer SNACK = 4;

	/***************************************************
	 * Especificação
	 ***************************************************/

	// Bolos e tortas
	public static final Integer SALTY_CAKE = 1;
	public static final Integer CHOCOLATE_CAKE = 2;
	public static final Integer CAKE_TOP = 3;
	public static final Integer CAKE_FILLING = 4;

	// Carnes
	public static final Integer PESTLE = 1;
	public static final Integer BREADED_STEAK = 2;
	public static final Integer ROAST_BEEF = 3;
	public static final Integer BARBECUE = 4;

	// Doce / Sobremesa
	public static final Integer MOUSSE = 1;
	public static final Integer PUDDING = 2;
	public static final Integer MILK_CANDY = 3;
	public static final Integer MILK_BRAID = 4;

	// Lanche
	public static final Integer SANDWICH = 1;
	public static final Integer DONUT = 2;
	public static final Integer GINGERBREAD = 3;
	public static final Integer BREAD = 4;
}
