package com.ufsc.enc.tcc.server.util;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONOperations {

	public static JSONObject documentToJSONObject(Document document) {
		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(document.toJson());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (jsonObject == null) {
			return new JSONObject();
		} else {
			return jsonObject;
		}
	}

	public static Document jsonObjectToDocument(JSONObject jsonObject) {
		Document document = Document.parse(jsonObject.toString());

		return document;
	}

	public static List<JSONObject> documentListToJSONObjectList(List<Document> documents) {
		List<JSONObject> jsonObjects = new ArrayList<>();

		for (Document document : documents) {
			jsonObjects.add(documentToJSONObject(document));
		}

		return jsonObjects;
	}

	public static List<Document> jsonObjectToDocument(List<JSONObject> jsonObjects) {

		List<Document> documents = new ArrayList<>();

		for (JSONObject jsonObject : jsonObjects) {
			documents.add(jsonObjectToDocument(jsonObject));
		}

		return documents;
	}
}
