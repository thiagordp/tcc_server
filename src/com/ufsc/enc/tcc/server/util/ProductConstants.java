package com.ufsc.enc.tcc.server.util;

public class ProductConstants {

	/**
	 * 
	 */
	public static final String barCodeKey = "bar_code";

	/**
	 * 
	 */
	public static final String descriptionKey = "description";

	public static final String manufacturerKey = "manufacturer";

	public static final String priceKey = "price";

	public static final String nutriInformationKey = "nutritional_info";

	public static final String kcalKey = "kcal";

	public static final String totalFatKey = "total_fat";

	public static final String timestampKey = "timestamp";

	public static final String sodiumKey = "sodium";

	public static final String proteinKey = "protein";

	public static final String refAmmountKey = "reference";

	public static final String expirationExpectation = "expiration_expec";

	public static final String weightVolumeKey = "weight_volume";

	public static final String productClassKey = "class";

	public static final String productEspecificationKey = "especfication";

	public static final String availableAmmountKey = "avaible_quantity";

	public static final String classificationKey = "classification";

	public static final String epcKey = "epc";

	public static final String epcHeaderKey = "header";

	public static final String epcManufacturerKey = "manufacturer";

	public static final String epcProductKey = "product";

	public static final String urlKey = "url";

}
