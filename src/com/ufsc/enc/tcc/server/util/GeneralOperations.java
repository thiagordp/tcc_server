package com.ufsc.enc.tcc.server.util;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.database.MetaInformationBasis;
import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.model.EPC;
import com.ufsc.enc.tcc.server.util.Constants;

public class GeneralOperations
{

	public static Timestamp getHoursAgoTimestamp(Integer hours)
	{

		Long subtractTime = (long) ((hours * 3600) * 1000);

		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());

		// System.out.println(timestamp);
		timestamp.setTime(timestamp.getTime() - subtractTime);
		// System.out.println(timestamp);

		return timestamp;
	}

	public static JSONObject getCurrentproductsByInteractions(List<JSONObject> listInteractions, MongoDB mongoDB)
	{
		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(mongoDB);
		JSONObject jsonProducts = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		HashSet<BigInteger> epcCodes = new HashSet<>();

		System.out.println("ListInt\t" + listInteractions);

		// Resgate dos códigos EPC da interação e inserção no conjunto Hash de código
		for (JSONObject json : listInteractions) {

			try {
				jsonArray = json.getJSONArray(Constants.EPC_CODES_KEY);

				for (int i = 0; i < jsonArray.length(); i++) {
					String tmp = jsonArray.getString(i);
					System.out.println(tmp);
					epcCodes.add(new BigInteger(tmp));
				}
			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// Nesse ponto tem-se uma lista de todos os produtos que interagiu nesse tempo
		jsonArray = new JSONArray();

		// Pegar a metaInformação de cada produto.
		System.out.println("Epccodes\t" + epcCodes);

		for (BigInteger epcCode : epcCodes) {

			// Resgatar código de barras a partir do EPC
			EPC epc = new EPC(epcCode);

			String barCode = "";
			try {
				JSONObject jsonMeta = metaInformationBasis.getProductMetaInfoByEPC(epc.getEPCManagerNumber(),
						epc.getObjectClass());

				if (jsonMeta == null) { // Caso não encontre, pula pro próximo epc.
					continue;
				}

				// System.out.println("JSON_META: " + jsonMeta.toString());
				barCode = jsonMeta.getString(Constants.PRODUCT_BARCODE_KEY);

				boolean found = false;

				// Verifica se já foi adicionado
				for (int i = 0; i < jsonArray.length(); i++) {

					JSONObject jsonObj = jsonArray.getJSONObject(i);

					if (jsonObj.getString(Constants.PRODUCT_BARCODE_KEY).equals(barCode)) {
						found = true;

						Integer quantity = jsonObj.getInt(Constants.PRODUCT_QUANTITY_KEY);
						jsonObj.put(Constants.PRODUCT_QUANTITY_KEY, quantity + 1);

						break;
					}
				}

				if (!found) {
					JSONObject json = new JSONObject();
					json.put(Constants.PRODUCT_BARCODE_KEY, barCode);
					json.put(Constants.PRODUCT_QUANTITY_KEY, 1);

					jsonArray.put(json);
				}

			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		try {
			jsonProducts.put(Constants.PRODUCT_LIST_KEY, jsonArray);
			System.out.println("PRODUCTS:\t" + jsonProducts);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		return jsonProducts;
	}

	public static String getTimestamp()
	{
		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		return timestamp.toString();
	}

	public static void showTime(Long timeNano)
	{
		System.out.print("Tempo de execução:\t");

		Double nanoTime = Double.valueOf(timeNano);

		if (nanoTime > 1000 * 1000 * 1000) { // segundos
			nanoTime = nanoTime / (1000 * 1000 * 1000);
			System.out.format("%.2fs\n", nanoTime);
		}
		else if (nanoTime > 1000 * 1000) { //
			nanoTime = nanoTime / (1000 * 1000);
			System.out.format("%.2fms\n", nanoTime);
		}
		else if (nanoTime > 1000) {
			nanoTime = nanoTime / (1000);
			System.out.format("%.2fus\n", nanoTime);
		}
		else {
			System.out.format("%.2fns\n", nanoTime);
		}
	}

	public static JSONObject JSONMessage(String message)
	{
		JSONObject json = new JSONObject();

		try {
			json.put("message", message);
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return json;
	}

	public static BigInteger generateEPC(Long header, Long manufacturer, Long product, Long serial)
	{
		BigInteger epc = new BigInteger("0");

		BigInteger epcHeader = new BigInteger(header.toString());
		epcHeader = epcHeader.shiftLeft(Constants.EPC_BIT_HEADER);

		BigInteger epcManufacturer = new BigInteger(manufacturer.toString());
		epcManufacturer = epcManufacturer.shiftLeft(Constants.EPC_BIT_MANUFACTURER);

		BigInteger epcProduct = new BigInteger(product.toString());
		epcProduct = epcProduct.shiftLeft(Constants.EPC_BIT_PRODUCT);

		BigInteger epcSerial = new BigInteger(serial.toString());

		epc = epc.add(epcHeader);
		epc = epc.add(epcManufacturer);
		epc = epc.add(epcProduct);
		epc = epc.add(epcSerial);

		return epc;
	}
}
