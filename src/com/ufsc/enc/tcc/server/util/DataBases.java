package com.ufsc.enc.tcc.server.util;

import java.util.ArrayList;
import java.util.List;

public class DataBases {

	/**
	 * 
	 */
	public final static String DATABASE_NAME = "tcc_server";

	/**
	 * 
	 */
	public final static String INTERACTION_BASE_NAME = "interaction_base";

	/**
	 * 
	 */
	public final static String AUX_STRUCT_BASE_NAME = "auxiliar_structure";

	/**
	 * 
	 */
	public final static String META_INFO_BASE_NAME = "meta_information";

	/**
	 * 
	 */
	public final static String RECOMMENDATION_BASE_NAME = "recommendation";

	/**
	 * 
	 * @return
	 */
	public static List<String> getCollections() {

		List<String> list = new ArrayList<String>();

		list.add(INTERACTION_BASE_NAME);
		list.add(AUX_STRUCT_BASE_NAME);
		list.add(META_INFO_BASE_NAME);
		list.add(RECOMMENDATION_BASE_NAME);

		return list;
	}
}
