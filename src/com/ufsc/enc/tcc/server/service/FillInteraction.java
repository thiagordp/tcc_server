/**
 * 
 */
package com.ufsc.enc.tcc.server.service;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.database.InteractionBasis;
import com.ufsc.enc.tcc.server.database.MetaInformationBasis;
import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.util.Constants;

/**
 * @author trdp
 *
 */
public class FillInteraction {
	private MongoDB mongoDB;

	public FillInteraction(MongoDB mongoDB) {
		this.mongoDB = mongoDB;
	}

	public void fillEntries() {

		InteractionBasis interactionBasis = new InteractionBasis(this.mongoDB);
		JSONObject interaction;
		JSONArray epcCodes;
		Random random = new Random(System.nanoTime());

		System.out.println("Preenchendo base de interações");

		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);
		List<JSONObject> listProduct = metaInformationBasis.getAllProductMetaInfo();
		System.out.println(listProduct);

		for (int i = 1; i <= Constants.FRIDGE_COUNT; i++) {

			Timestamp timestampGlobal = new Timestamp(new Date().getTime() - (150 * 3600 * 1000));

			System.out.println("global:" + timestampGlobal.toString());

			for (int j = 0; j < Constants.MAX_INTERACTION; j += random.nextInt(5) + 1) {
				interaction = new JSONObject();
				epcCodes = new JSONArray();

				for (int k = 0; k < 10; k += random.nextInt(15) + 1) {

					BigInteger epc = new BigInteger("0");
					JSONObject tmpProduct = listProduct.get(random.nextInt(listProduct.size()));

					Integer code;

					try {
						System.out.println(tmpProduct);
						JSONObject tmpEPC = tmpProduct.getJSONObject(Constants.EPC_KEY);

						//////////////////////////////////////////////////////////
						// OBTENÇÃO DO CÓDIGO EPC A PARTIR DOS DADOS DO PRODUTO //
						//////////////////////////////////////////////////////////

						BigInteger epcHeader = new BigInteger(Constants.EPC_HEADER.toString());
						epcHeader = epcHeader.shiftLeft(Constants.EPC_BIT_HEADER);

						code = tmpEPC.getInt(Constants.EPC_MANUFACTURER_KEY);
						BigInteger epcManufacturer = new BigInteger(code.toString());
						epcManufacturer = epcManufacturer.shiftLeft(Constants.EPC_BIT_MANUFACTURER);

						code = tmpEPC.getInt(Constants.EPC_PRODUCT_KEY);
						BigInteger epcProduct = new BigInteger(code.toString());
						epcProduct = epcProduct.shiftLeft(Constants.EPC_BIT_PRODUCT);

						BigInteger epcSerial = new BigInteger(36, random);

						epc = epc.add(epcHeader);
						epc = epc.add(epcManufacturer);
						epc = epc.add(epcProduct);
						epc = epc.add(epcSerial);

						// System.out.println(tmpProduct);
						// System.out.println("HEADER:\t" + epcHeader.toString());
						// System.out.println("MAN:\t" + epcManufacturer.toString());
						// System.out.println("PROD:\t" + epcProduct.toString());
						// System.out.println("INS:\t" + epcSerial.toString());
						// System.out.println("EPC:\t" + epc.toString());

						epcCodes.put(epc.toString());

						System.out.println("\n----------------------------\nINTERACTION:\t" + epc + "\t\t" + tmpProduct
								+ "\n----------------------------\n");

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				// Timestamp timestamp =
				// GeneralOperations.getHoursAgoTimestamp(Constants.MAX_INTERACTION - j);

				try {
					interaction.put(Constants.EPC_CODES_KEY, epcCodes);
					interaction.put(Constants.ID_FRIDGE_KEY, i);
					interaction.put(Constants.TIMESTAMP_KEY, timestampGlobal.toString());
					System.out.println("TIMESTAMP..." + timestampGlobal);

					timestampGlobal = new Timestamp(timestampGlobal.getTime() + (random.nextInt(30) + 10) * 60 * 1000);

				} catch (JSONException e) {
					e.printStackTrace();
				}

				// interactionBasis.insertInteraction(interaction);

				for (int l = 0; l < 5; l++) {

					if (random.nextInt(101) > 80) {
						try {

							interaction.remove(Constants.TIMESTAMP_KEY);
							interaction.put(Constants.TIMESTAMP_KEY, timestampGlobal.toString());
							interactionBasis.insertInteraction(interaction);

							timestampGlobal = new Timestamp(
									timestampGlobal.getTime() + (random.nextInt(80) + 10) * 60 * 1000);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

			}
		}

		System.out.println("Interacoes:\t" + interactionBasis.listAllInteractions());
	}
}
