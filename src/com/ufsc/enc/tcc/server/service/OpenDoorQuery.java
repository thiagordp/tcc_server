/**
 * 
 */
package com.ufsc.enc.tcc.server.service;

import org.json.JSONObject;

import com.ufsc.enc.tcc.server.database.AuxiliaryStructureBasis;
import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.util.Constants;

/**
 * @author trdp
 *
 */
public class OpenDoorQuery {
	private MongoDB mongoDB;

	public OpenDoorQuery(MongoDB mongoDB) {
		super();
		this.mongoDB = mongoDB;
	}

	public String getStatusDoor(Integer idFridge) {
		AuxiliaryStructureBasis auxiliaryStructureBasis = new AuxiliaryStructureBasis(this.mongoDB);

		JSONObject json =  auxiliaryStructureBasis.getDoorStatus(idFridge);
		json.remove(Constants.DEFAULT_ID);
		json.remove(Constants.RECORD_TYPE_KEY);
		
		
		return json.toString();
	}
}
