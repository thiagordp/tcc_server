/**
 * 
 */
package com.ufsc.enc.tcc.server.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import com.ufsc.enc.tcc.server.database.MetaInformationBasis;
import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.util.Constants;
import com.ufsc.enc.tcc.server.util.DataBases;
import com.ufsc.enc.tcc.server.util.JSONOperations;

/**
 * @author trdp
 */
public class PurchaseRecommenderQuery {
	private MongoDB mongoDB;

	public PurchaseRecommenderQuery(MongoDB mongoDB) {
		this.mongoDB = mongoDB;
	}

	public String getPurchaseRecommendation(Integer idFridge) {
		int MAX_REC = 10;
		int count = 0;

		Document document = new Document();
		JSONArray jsonOutRec = new JSONArray();

		/**
		 * Produto novo
		 ***/
		document.put(Constants.ID_FRIDGE_KEY, idFridge);
		document.put(Constants.RECOMMENDATION_TYPE_KEY, Constants.PRODUCT_REC_NEW);
		List<Document> tmplistRec = this.mongoDB.search(document, DataBases.RECOMMENDATION_BASE_NAME);
		List<JSONObject> listRecNew = JSONOperations.documentListToJSONObjectList(tmplistRec);

		System.out.println("LIST_REC_NEW:\t" + listRecNew);

		document.clear();
		document.put(Constants.ID_FRIDGE_KEY, idFridge);
		List<Document> tmpList = this.mongoDB.search(document, DataBases.RECOMMENDATION_BASE_NAME);
		List<JSONObject> jsonList = JSONOperations.documentListToJSONObjectList(tmpList);

		System.out.println("LIST:\t" + jsonList);

		for (JSONObject doc : listRecNew) {

			if (count == MAX_REC)
				break;
			JSONObject json = new JSONObject();

			JSONArray tmpArray;
			try {
				tmpArray = doc.getJSONArray(Constants.PRODUCT_LIST_KEY);

				for (int i = 0; i < tmpArray.length(); i++) {
					try {
						if (count == MAX_REC) {
							break;
						}

						JSONObject jsonProd = tmpArray.getJSONObject(i);
						System.out.println("DOC:\t" + jsonProd.toString());
						json = new JSONObject();
						json.put(Constants.PRODUCT_DESCRIPTION_KEY, jsonProd.get(Constants.PRODUCT_DESCRIPTION_KEY));
						json.put(Constants.PRODUCT_URL_IMG_KEY, jsonProd.get(Constants.PRODUCT_URL_IMG_KEY));
						json.put(Constants.PRODUCT_QUANTITY_KEY, jsonProd.get(Constants.PRODUCT_QUANTITY_KEY));
						json.put(Constants.RECOMMENDATION_TYPE_KEY, "Novo");
						jsonOutRec.put(json);

						count++;

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}

		System.out.println("OUTPUT > " + jsonOutRec.toString());

		/**
		 * Produto antigo
		 */
		document = new Document();
		document.put(Constants.ID_FRIDGE_KEY, idFridge);
		document.put(Constants.RECOMMENDATION_TYPE_KEY, Constants.PRODUCT_REC_MISS);
		tmplistRec = this.mongoDB.search(document, DataBases.RECOMMENDATION_BASE_NAME);
		listRecNew = JSONOperations.documentListToJSONObjectList(tmplistRec);

		System.out.println("LIST_REC_MISS:\t" + listRecNew);
		count = 0;
		for (JSONObject doc : listRecNew) {
			if (count == MAX_REC) {
				break;
			}

			try {
				JSONArray jsonProds = doc.getJSONArray(Constants.PRODUCT_LIST_KEY);

				for (int j = 0; j < jsonProds.length(); j++) {

					if (count == MAX_REC) {
						break;
					}

					JSONObject json = new JSONObject();
					JSONObject jsonJ = jsonProds.getJSONObject(j);

					System.out.println("DOC:\t" + jsonJ.toString());
					String recType = jsonJ.getString(Constants.RECOMMENDATION_TYPE_KEY);

					if (recType.equals(Constants.PRODUCT_REC_SIMILAR)) {

						MetaInformationBasis metaInformationBasis = new MetaInformationBasis(mongoDB);

						if (jsonJ.has(Constants.ALTERNATIVE_PRODUCT)) {

							String barCode = jsonJ.getJSONObject(Constants.ALTERNATIVE_PRODUCT)
									.getString(Constants.PRODUCT_BARCODE_KEY);

							JSONObject jsonMeta = metaInformationBasis.getProductMetaInfo(barCode);

							json.put(Constants.PRODUCT_DESCRIPTION_KEY, jsonMeta.get(Constants.PRODUCT_DESCRIPTION_KEY)
									+ " " + jsonMeta.getString(Constants.PRODUCT_MANUFACTURER_KEY));
							json.put(Constants.PRODUCT_BARCODE_KEY, jsonMeta.getString(Constants.PRODUCT_BARCODE_KEY));
							json.put(Constants.PRODUCT_URL_IMG_KEY, jsonMeta.get(Constants.PRODUCT_URL_IMG_KEY));
							json.put(Constants.PRODUCT_QUANTITY_KEY, jsonJ.get(Constants.PRODUCT_QUANTITY_KEY));

							json.put(Constants.RECOMMENDATION_TYPE_KEY, "Alternativa");

							barCode = jsonJ.getJSONObject(Constants.ORIGINAL_PRODUCT)
									.getString(Constants.PRODUCT_BARCODE_KEY);

							JSONObject jsonOriginMeta = metaInformationBasis.getProductMetaInfo(barCode);
							JSONObject jsonOrigin = new JSONObject();

							jsonOrigin.put(Constants.PRODUCT_BARCODE_KEY,
									jsonOriginMeta.get(Constants.PRODUCT_BARCODE_KEY));
							jsonOrigin.put(Constants.PRODUCT_URL_IMG_KEY,
									jsonOriginMeta.get(Constants.PRODUCT_URL_IMG_KEY));
							jsonOrigin.put(Constants.PRODUCT_DESCRIPTION_KEY,
									jsonOriginMeta.get(Constants.PRODUCT_DESCRIPTION_KEY) + " "
											+ jsonOriginMeta.getString(Constants.PRODUCT_MANUFACTURER_KEY));

							json.put(Constants.ORIGINAL_PRODUCT, jsonOrigin);

							System.out.println("RECOMMENDATION = " + json.toString());
							jsonOutRec.put(json);

							count++;

						} else {
							System.out.println("JSONJ:\t" + jsonJ);
							String barCode = jsonJ.getJSONObject(Constants.ORIGINAL_PRODUCT)
									.getString(Constants.PRODUCT_BARCODE_KEY);

							JSONObject jsonMeta = metaInformationBasis.getProductMetaInfo(barCode);

							json.put(Constants.PRODUCT_DESCRIPTION_KEY, jsonMeta.get(Constants.PRODUCT_DESCRIPTION_KEY)
									+ " " + jsonMeta.getString(Constants.PRODUCT_MANUFACTURER_KEY));
							json.put(Constants.PRODUCT_URL_IMG_KEY, jsonMeta.get(Constants.PRODUCT_URL_IMG_KEY));
							json.put(Constants.PRODUCT_QUANTITY_KEY, jsonJ.get(Constants.PRODUCT_QUANTITY_KEY));

							json.put(Constants.RECOMMENDATION_TYPE_KEY, "Sem alternativas");
							System.out.println("RECOMMENDATION2 = " + json.toString());
							jsonOutRec.put(json);

							count++;
						}
					} else if (recType.equals(Constants.PRODUCT_REC_OLD)) {
						System.out.println("OLD");

						String barCode = jsonJ.getString(Constants.PRODUCT_BARCODE_KEY);

						MetaInformationBasis metaInformationBasis = new MetaInformationBasis(mongoDB);
						JSONObject jsonMeta = metaInformationBasis.getProductMetaInfo(barCode);

						json.put(Constants.PRODUCT_DESCRIPTION_KEY, jsonMeta.get(Constants.PRODUCT_DESCRIPTION_KEY)
								+ " " + jsonMeta.getString(Constants.PRODUCT_MANUFACTURER_KEY));
						json.put(Constants.PRODUCT_URL_IMG_KEY, jsonMeta.get(Constants.PRODUCT_URL_IMG_KEY));
						json.put(Constants.PRODUCT_QUANTITY_KEY, jsonJ.get(Constants.PRODUCT_QUANTITY_KEY));

						json.put(Constants.RECOMMENDATION_TYPE_KEY, "Reposição");
						System.out.println("RECOMMENDATION3 = " + json.toString());
						jsonOutRec.put(json);

						count++;

					} else {
						System.out.println("1234asdf");
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Produto similar
		 ***/
		/*
		 * document = new Document(); document.put(Constants.ID_FRIDGE_KEY, idFridge);
		 * document.put(Constants.RECOMMENDATION_TYPE_KEY, Constants.PRODUCT_REC_MISS);
		 * tmplistRec = this.mongoDB.search(document,
		 * DataBases.RECOMMENDATION_BASE_NAME); listRecNew =
		 * JSONOperations.documentListToJSONObjectList(tmplistRec);
		 * 
		 * System.out.println("LIST_REC_SIMILAR:\t" + listRecNew); count = 0;
		 * 
		 * for (JSONObject doc : listRecNew) { if (count == MAX_REC) { break; }
		 * 
		 * JSONObject json = new JSONObject();
		 * 
		 * try { System.out.println("DOC:\t" + doc.toString());
		 * json.put(Constants.PRODUCT_DESCRIPTION_KEY,
		 * doc.get(Constants.PRODUCT_DESCRIPTION_KEY));
		 * json.put(Constants.PRODUCT_URL_IMG_KEY,
		 * doc.get(Constants.PRODUCT_URL_IMG_KEY));
		 * json.put(Constants.PRODUCT_QUANTITY_KEY,
		 * doc.get(Constants.PRODUCT_QUANTITY_KEY));
		 * json.put(Constants.RECOMMENDATION_TYPE_KEY, "Similar"); jsonOutRec.put(json);
		 * 
		 * count++;
		 * 
		 * } catch (JSONException e) { e.printStackTrace(); } }
		 */
		HashMap<String, JSONObject> hashMapProd = new HashMap<>();

		for (int i = 0; i < jsonOutRec.length(); i++) {

			try {

				JSONObject tmp;
				tmp = jsonOutRec.getJSONObject(i);
				String nome = tmp.getString(Constants.PRODUCT_DESCRIPTION_KEY);
				hashMapProd.put(nome, tmp);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		jsonOutRec = new JSONArray();
		for (String key : hashMapProd.keySet()) {
			JSONObject json = hashMapProd.get(key);
			jsonOutRec.put(json);
		}

		JSONObject json = new JSONObject();
		try {
			json.put(Constants.PRODUCT_LIST_KEY, jsonOutRec);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		System.out.println("FINAL SIM REC:\t" + json.toString());
		return json.toString();
	}

}
