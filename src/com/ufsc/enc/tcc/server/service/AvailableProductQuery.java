package com.ufsc.enc.tcc.server.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.database.InteractionBasis;
import com.ufsc.enc.tcc.server.database.MetaInformationBasis;
import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.util.Constants;
import com.ufsc.enc.tcc.server.util.GeneralOperations;

/**
 * Classe do serviço de verificação de disponibilidade de produtos.
 * @author trdp
 *
 */
public class AvailableProductQuery
{

	private MongoDB mongoDB;

	public AvailableProductQuery(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	/**
	 * Retorna os produtos disponíveis atualmente na geladeira
	 * @param idFridge Identificação da geladeira
	 * @return JSON contendo uma lista de produtos existentes.
	 */
	public String getAvailableProducts(Integer idFridge)
	{
		InteractionBasis interactionBasis = new InteractionBasis(this.mongoDB);			// Instância da base de interações
		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);

		JSONObject json = interactionBasis.getLastInteraction(idFridge);				// Seleciona o último e mais recente registro 
		List<JSONObject> listJson = new ArrayList<>();
		listJson.add(json);

		if (json == null) {
			return "{}";
		}

		JSONObject jsonObj = GeneralOperations.getCurrentproductsByInteractions(listJson, this.mongoDB);
		System.out.println("JSON_FULL\t" + jsonObj);

		JSONArray outArray = new JSONArray();
		JSONObject outObj = new JSONObject();

		try {
			JSONArray jsonArray = jsonObj.getJSONArray(Constants.PRODUCT_LIST_KEY);

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonProdCount = jsonArray.getJSONObject(i);
				String barcode = jsonProdCount.getString(Constants.PRODUCT_BARCODE_KEY);
				JSONObject metaInfoJSON = metaInformationBasis.getProductMetaInfo(barcode);

				JSONObject tmpProd = new JSONObject();
				tmpProd.put(Constants.PRODUCT_BARCODE_KEY, barcode);
				tmpProd.put(Constants.PRODUCT_DESCRIPTION_KEY,
						metaInfoJSON.getString(Constants.PRODUCT_DESCRIPTION_KEY));
				tmpProd.put(Constants.PRODUCT_QUANTITY_KEY, jsonProdCount.get(Constants.PRODUCT_QUANTITY_KEY));
				tmpProd.put(Constants.PRODUCT_URL_IMG_KEY, metaInfoJSON.get(Constants.PRODUCT_URL_IMG_KEY));
				tmpProd.put(Constants.PRODUCT_MANUFACTURER_KEY, metaInfoJSON.get(Constants.PRODUCT_MANUFACTURER_KEY));

				outArray.put(tmpProd);
			}

			outObj.put(Constants.PRODUCT_LIST_KEY, outArray);

			System.out.println("OUTOBJ\t" + outObj.toString());
			return outObj.toString();
		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		return "{}";
	}
}
