package com.ufsc.enc.tcc.server.service;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.database.AuxiliaryStructureBasis;
import com.ufsc.enc.tcc.server.database.InteractionBasis;
import com.ufsc.enc.tcc.server.database.MetaInformationBasis;
import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.database.RecommendationBasis;
import com.ufsc.enc.tcc.server.util.Constants;

public class FillDatabase
{

	private MongoDB mongoDB; //= new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

	public FillDatabase(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	public void fillInteractionBase()
	{
		FillInteraction fillInteraction = new FillInteraction(this.mongoDB);
		fillInteraction.fillEntries();
	}

	public void eraseInteractionBase()
	{
		InteractionBasis interactionBasis = new InteractionBasis(this.mongoDB);
		interactionBasis.removeAllInteractions();
	}

	public void fillAuxiliaryBase()
	{

		AuxiliaryStructureBasis auxiliaryStructureBasis = new AuxiliaryStructureBasis(this.mongoDB);

		// Inicialização do estado atual da porta
		for (int i = 1; i <= Constants.FRIDGE_COUNT; i++) {
			auxiliaryStructureBasis.setDoorStatus(i, Constants.DOOR_CLOSED);
		}

		// Inicialização da lista de compras
		// TODO: Após implementar o processo de compras, implementar essa inicialização

		/////////////////////////////////////////////////////////
		//  Inicialização das configurações de cada geladeira  //
		/////////////////////////////////////////////////////////

		JSONObject settings;
		JSONArray products;
		JSONObject market;
		JSONObject resources, productInfo;
		Random random = new Random(System.nanoTime());

		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);
		List<JSONObject> listProducts = metaInformationBasis.getAllProductMetaInfo();

		for (int i = 1; i <= Constants.FRIDGE_COUNT; i++) {
			settings = new JSONObject();
			products = new JSONArray();
			market = new JSONObject();
			resources = new JSONObject();

			try {
				settings.put(Constants.ID_FRIDGE_KEY, i);
				settings.put(Constants.RECORD_TYPE_KEY, Constants.RECORD_TYPE_SETTINGS_KEY);

				HashSet<JSONObject> finalList = new HashSet<>();  // Tipo de lista que não permite duplicatas e tem acesso rápido.

				if (listProducts.size() > 0) {
					for (int j = 0; j < 5; j++) {
						Integer index = random.nextInt(listProducts.size());
						finalList.add(listProducts.get(index));
					}

					for (JSONObject json : finalList) {
						String barCode = json.getString(Constants.PRODUCT_BARCODE_KEY);

						productInfo = new JSONObject();
						productInfo.put(Constants.PRODUCT_BARCODE_KEY, barCode);
						productInfo.put(Constants.PRODUCT_QUANTITY_KEY, random.nextInt(20) + 1);
						productInfo.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "UN");

						products.put(productInfo);
					}
				}
				System.out.println("PRODUCTS:\t" + products.toString());
				settings.put(Constants.PRODUCT_LIST_KEY, products);

				market.put(Constants.HOSTNAME_KEY, "192.168.1.202");
				market.put(Constants.PORT_KEY, 8080);
				market.put(Constants.PATH_NAME_KEY, "/tcc_market");

				resources.put(Constants.RESOURCE_AVAILABILITY_KEY, "/product_av");
				resources.put(Constants.RESOURCE_PURCHASE_KEY, "/purchase");
				resources.put(Constants.RESOURCE_SYNC_METAINFO_KEY, "/sync_database");
				//market.put(Constants.RESOURCE_NAME_KEY, "/filldatabases");
				market.put(Constants.RESOURCES_KEY, resources);
				settings.put(Constants.MARKET_KEY, market);

				settings.put(Constants.NOTIFICATION_DOOR_PERIOD_KEY, 30);
				settings.put(Constants.PURCHASE_PERIOD_KEY, 7200); // 2 Horas

				auxiliaryStructureBasis.editSettings(settings);
				System.out.println(auxiliaryStructureBasis.getSettings(i));
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void eraseAuxiliaryBase()
	{
		AuxiliaryStructureBasis auxiliaryStructureBasis = new AuxiliaryStructureBasis(this.mongoDB);
		auxiliaryStructureBasis.removeAllAuxiliary();
	}

	public void fillRecommendationBase()
	{

	}

	public void eraseRecommendationBase()
	{
		RecommendationBasis recommendationBasis = new RecommendationBasis(this.mongoDB);
		recommendationBasis.removeAllRecommendation();
	}

	public void fillMetaInfoBase()
	{
		FillMetaInfo fillMetaInfo = new FillMetaInfo(this.mongoDB);
		fillMetaInfo.fillEntries();

		FillRecipe fillRecipe = new FillRecipe(this.mongoDB);
		fillRecipe.fillEntries();
	}

	public void eraseMetaInfoBase()
	{
		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);
		metaInformationBasis.removeAllMetainformation();
	}
}
