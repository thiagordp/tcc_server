package com.ufsc.enc.tcc.server.service;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.database.RecommendationBasis;
import com.ufsc.enc.tcc.server.util.Constants;

public class RecipeRecommendQuery
{
	private MongoDB mongoDB;

	public RecipeRecommendQuery(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	public String getRecipeRecommendation(Integer idFridge)
	{
		Integer countRec = 5;
		RecommendationBasis recommendationBasis = new RecommendationBasis(this.mongoDB);

		List<JSONObject> listObj = recommendationBasis.getRecipeRecommendation(idFridge, countRec);
		
		JSONObject json = new JSONObject();
		JSONArray jsonRecipes = new JSONArray(listObj);

		try {
			for (JSONObject jsonObj : listObj) {
				JSONArray jsonRecipesTmp = jsonObj.getJSONArray(Constants.PRODUCT_LIST_KEY);

				for (int j = 0; j < jsonRecipesTmp.length(); j++) {
					if (jsonRecipes.length() > countRec) {
						break;
					}

					jsonRecipes.put(jsonRecipesTmp.getJSONObject(j));
				}
			}

			json.put(Constants.PRODUCT_LIST_KEY, jsonRecipes);

			return json.toString();

		}
		catch (JSONException e) {

			e.printStackTrace();
		}

		return null;
	}

}
