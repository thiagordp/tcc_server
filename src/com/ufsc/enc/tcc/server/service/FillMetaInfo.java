package com.ufsc.enc.tcc.server.service;

import java.sql.Timestamp;
import java.util.Date;

import org.bson.Document;

import com.ufsc.enc.tcc.server.database.MetaInformationBasis;
import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.util.Constants;
import com.ufsc.enc.tcc.server.util.JSONOperations;

public class FillMetaInfo
{

	private MongoDB mongoDB;

	public FillMetaInfo(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	/**
	 * 
	 * EPC Fabricantes
	 */
	private final Integer VigorEPC = 1;
	private final Integer TirolEPC = 2;
	private final Integer LangaruEPC = 3;
	private final Integer QualyEPC = 4;
	private final Integer ParmalatEPC = 5;
	private final Integer GloriaEPC = 6;
	private final Integer PiaEPC = 7;
	private final Integer RitterEPC = 8;
	private final Integer DanoneEPC = 9;
	private final Integer BatavoEPC = 10;
	private final Integer ActiviaEPC = 11;
	private final Integer SadiaEPC = 12;
	private final Integer SulfriosEPC = 13;
	private final Integer PerdigaoEPC = 14;
	private final Integer AntarticaEPC = 15;
	private final Integer BudweiserEPC = 16;
	private final Integer PepsiEPC = 17;
	private final Integer FantaEPC = 18;
	private final Integer CocaColaEPC = 19;
	private final Integer GolgMeatEPC = 20;
	private final Integer SearaEPC = 21;
	private final Integer MarfrigEPC = 22;
	private final Integer TopQualityEPC = 23;
	private final Integer AuroraEPC = 24;

	/**
	 * Classe
	 */
	private final Integer laticinio = 1;
	private final Integer embutido = 2;
	private final Integer bebida = 3;
	private final Integer congelado = 4;
	private final Integer carne = 5;

	/**
	 * Produtos
	 */
	private final Integer leite = 1;
	private final Integer margarina = 2;
	private final Integer leiteCondensado = 3;
	private final Integer cremeDeLeite = 4;
	private final Integer doce = 5;
	private final Integer iogurte = 6;
	private final Integer queijoMussarela = 7;
	private final Integer mortadela = 8;
	private final Integer cerveja = 9;
	private final Integer refrigerante = 10;
	private final Integer pizza = 11;
	private final Integer linguica = 12;
	private final Integer iscasCarne = 13;
	private final Integer bife = 14;
	private final Integer maminha = 15;
	private final Integer lasanha = 16;
	private final Integer salsicha = 17;

	/**
	 * Insere os registros dos produtos
	 * 
	 * @return
	 */
	public String fillEntries()
	{
		MetaInformationBasis database = new MetaInformationBasis(this.mongoDB);

		Document document = new Document();
		Document classification = new Document();
		Document nutritionalInfo = new Document();
		Document epcCode = new Document();
		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896256601848");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Leite integral");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Tirol");
		document.append(Constants.PRODUCT_PRICE_KEY, 1.99F);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 4);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "1L");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 150);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://www.tirol.com.br/media/cache/product_vertical/files/product/1506d7c617f17e34ce63d60adcacb249.png");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 600); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 30); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 650); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 30); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "1L");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, leite);
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, TirolEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x01);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891164028237");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Leite integral");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Aurora");
		document.append(Constants.PRODUCT_PRICE_KEY, 2.37F);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 4);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "1L");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 100);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://www.hippo.com.br/uploads/Produto/97451/Default/grande_15ed59e88f33a766b95897284ecd37ed.jpeg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 580); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 30); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 760); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 31.5); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "1L");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, leite);
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, AuroraEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x01);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896648699453");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Leite Integral");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Langaru");
		document.append(Constants.PRODUCT_PRICE_KEY, 2.29F);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 4);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "1L");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 130);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://www.hippo.com.br/uploads/Produto/97451/Default/grande_15ed59e88f33a766b95897284ecd37ed.jpeg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 590); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 30); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 500); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 30); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "1L");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, leite);
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, LangaruEPC); // Langaru
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x01);
		document.append(Constants.EPC_KEY, epcCode);

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896256603422");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Leite Desnatado");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Tirol");
		document.append(Constants.PRODUCT_PRICE_KEY, 1.99F);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 4);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "1L");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 100);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://www.tirol.com.br/files/product/329af4810345c8a376767a49017f370b.png");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 300); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 650); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 30); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "1L");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, leite);
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, TirolEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x02);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Qualy
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7893000394209");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Margarina com Sal");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Qualy");
		document.append(Constants.PRODUCT_PRICE_KEY, 5.95F);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 30);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "500g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 100);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"https://cdn.blink.com.br/Arquivos/BSI_Ipiranga/Arquivos/Produtos/ProdutoDetalhe/7893000394209.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 3600); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 400); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 3000); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "500g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, margarina); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, QualyEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x02);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891999011039");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Margarina com Sal");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Vigor");
		document.append(Constants.PRODUCT_PRICE_KEY, 4.55F);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 30);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "500g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 100);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7891999011039");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 3600); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 400); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 3000); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "500g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, margarina); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, VigorEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x02);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896034680010");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Leite Condensado");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Parmalat");
		document.append(Constants.PRODUCT_PRICE_KEY, 3.37F);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 7);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "395g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 130);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7896034680010");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 1264); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 31.6); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 355.5); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 29.625); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "395g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, leiteCondensado); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, ParmalatEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x90);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896256604559");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Leite Condensado");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Tirol");
		document.append(Constants.PRODUCT_PRICE_KEY, 3.95F);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 7);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "395g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 110);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7896034680010");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 65); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 1.7); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 21); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 1.4); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "20g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, leiteCondensado); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, TirolEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x1);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896256600780");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Creme de Leite");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Tirol");
		document.append(Constants.PRODUCT_PRICE_KEY, 2.55F);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 2);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "200g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 30);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7896256600780");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 26); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 2.6); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 11); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "15g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, cremeDeLeite); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, TirolEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x16);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896183210120");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Creme de Leite");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Glória");
		document.append(Constants.PRODUCT_PRICE_KEY, 2.39);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 2);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "200g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 79);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7896183210120");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 27); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 2.7); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 13); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "15g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, cremeDeLeite); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, GloriaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x198);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "789034630442");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Creme de Leite");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Parmalat");
		document.append(Constants.PRODUCT_PRICE_KEY, 1.89);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 4);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "200g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 60);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://www.mercadomirai.com.br/public/fotos/produto/p_22f46dccc9c917b.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 29); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 2.6); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 8); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0.6); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "15g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, cremeDeLeite); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, ParmalatEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x18);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896256600278");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Doce de leite");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Tirol");
		document.append(Constants.PRODUCT_PRICE_KEY, 4.99);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 15);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "350g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 36);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7896256600278");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 60); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 1.3); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 57); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 1.2); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "20g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, doce); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, TirolEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x16);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896348841039");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Doce de leite");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Piá");
		document.append(Constants.PRODUCT_PRICE_KEY, 8.15);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 30);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "400g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 73);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7896348841039");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 58); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 1.2); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 21); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 1.2); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "20g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, doce); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, PiaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x06);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896104802410");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Doce de abacaxi");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Ritter");
		document.append(Constants.PRODUCT_PRICE_KEY, 5.85);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 30);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "380g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 98);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://www.lojaritter.com.br/Assets/Produtos/SuperZoom/DOCEABACAXI400_636028965479797885.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 41); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 0); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "20g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, doce); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, RitterEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x23);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891025101376");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Iogurte Danone");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Danone");
		document.append(Constants.PRODUCT_PRICE_KEY, 1.77);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 1);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "170g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 200);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7896348841039");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 157); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 5.3); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 64); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 3.5); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "170g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, iogurte); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, DanoneEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x32);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891025101376");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Iogurte de Morango");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Danone");
		document.append(Constants.PRODUCT_PRICE_KEY, 1.77);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 1);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "170g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 200);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://mambo.vteximg.com.br/arquivos/ids/176473/192639_24530.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 157); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 5.3); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 64); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 3.5); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "170g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, iogurte); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, DanoneEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x32);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891515971915");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Iogurte de Morango");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Batavo");
		document.append(Constants.PRODUCT_PRICE_KEY, 1.29);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 1);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "180g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 163);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://mambo.vteximg.com.br/arquivos/ids/176473/192639_24530.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 140); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 1.8); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 67); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 3.9); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "180g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, iogurte); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, BatavoEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 0x82);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891025109396");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Iogurte de Morango");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Activia");
		document.append(Constants.PRODUCT_PRICE_KEY, 2.35);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 1);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "170g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 8166);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://delivery.supermuffato.com.br/arquivos/ids/226548-1000-1000/7891025109396.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 123); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 1.8); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 71); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 4.4); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "170g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, iogurte); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, ActiviaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 4534);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896256602050");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Iogurte de Morango");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Tirol");
		document.append(Constants.PRODUCT_PRICE_KEY, 2.25);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 1);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "170g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 263);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://delivery.supermuffato.com.br/arquivos/ids/226548-1000-1000/7891025109396.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 142); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 3.4); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 70); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 5.8); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "170g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, iogurte); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, TirolEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 1263);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7893000084315");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Queijo Mussarela");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Sadia");
		document.append(Constants.PRODUCT_PRICE_KEY, 5.45);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 5);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "200g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 342);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7893000084315");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 125); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 9.8); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 158); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 9.1); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "200g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, queijoMussarela); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, SadiaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 6354);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7893000135529");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Salsicha");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Sadia");
		document.append(Constants.PRODUCT_PRICE_KEY, 8.89);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 5);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "500g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 526);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7893000135529");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 121); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 9.5); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 399); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 6.8); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "50g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, embutido);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, salsicha); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, SadiaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 2634);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7893000084315");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Queijo Mussarela");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Sadia");
		document.append(Constants.PRODUCT_PRICE_KEY, 5.45);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 5);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "200g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 342);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7893000084315");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 125); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 9.8); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 158); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 9.1); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "200g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, queijoMussarela); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, SadiaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 6354);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7898329390956");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Queijo Mussarela");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Sulfrios");
		document.append(Constants.PRODUCT_PRICE_KEY, 4.95);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 5);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "130g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 362);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://www.sulfrios.com.br/midias/produto=76ee72a5b42de1d9695667282c3c7b8b.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 100); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 8); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 160); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 7); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "30g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, queijoMussarela); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, SulfriosEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 3625);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896256604440");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Queijo Mussarela");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Tirol");
		document.append(Constants.PRODUCT_PRICE_KEY, 6.79);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 8);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "150g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 70);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://www.tirol.com.br/files/product/281b480fd7a8e0d482016aee9706249b.png");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 57); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 4.2); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 70); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 4.2); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "18g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, laticinio);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, queijoMussarela); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, TirolEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 4237);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891515431945");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Mortadela");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Sadia");
		document.append(Constants.PRODUCT_PRICE_KEY, 3.57);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 5);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "400g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 96);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://www.sadia.com.br/sites/default/files/soltissimo-mortadela304x409.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 117); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 9.4); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 540); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 4.8); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "40g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, embutido);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, mortadela); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, SadiaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 3829);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891515429966");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Mortadela");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Perdigão");
		document.append(Constants.PRODUCT_PRICE_KEY, 3.15);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 5);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "400g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 124);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"https://www.confianca.com.br/media/catalog/product/cache/1/image/400x400/9df78eab33525d08d6e5fb8d27136e95/2/4/243493.jpg.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 120); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 9.6); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 540); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 5.2); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "40g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, embutido);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, mortadela); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, PerdigaoEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 7634);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////
/*
		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891991010023");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Cerveja Sub-Zero");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Ant�rtica");
		document.append(Constants.PRODUCT_PRICE_KEY, 1.97);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 7);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "350ml");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 329);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"https://cdn-cosmos.bluesoft.com.br/products/antarctica-sub-zero-pilsen-lata-350-ml-1-unidade_300x300-PU5d9d4_1.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 0); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 0); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "0ml");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, bebida);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, cerveja); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, AntarticaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 3241);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));*/

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////
/*
		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891991010481");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Cerveja");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Budweiser");
		document.append(Constants.PRODUCT_PRICE_KEY, 3.15);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 7);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "350ml");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 329);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"https://cdn-cosmos.bluesoft.com.br/products/budweiser-american-lager-lata-350-ml-1-unidade_300x300-PU5d9b5_1.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 0); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 0); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "0ml");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, bebida);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, cerveja); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, BudweiserEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 324);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));
*/
		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7892840800000");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Refrigerante");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Pepsi");
		document.append(Constants.PRODUCT_PRICE_KEY, 5.97);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 2);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "2L");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 136);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"https://www.magodrive.com.br/Imagem/produtos/7892840800000.png");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 0); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 0); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "0ml");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, bebida);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, refrigerante); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, PepsiEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 3218);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7894900093056");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Refrigerante de Guaraná");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Fanta");
		document.append(Constants.PRODUCT_PRICE_KEY, 4.79);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 2);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "2L");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 320);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"https://s3.amazonaws.com/vc-cidade/files/produtos/grande/7894900093056-adm-f-1.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 0); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 0); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "0ml");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, bebida);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, refrigerante); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, FantaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 362);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7894900011517");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Refrigerante");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Coca-Cola");
		document.append(Constants.PRODUCT_PRICE_KEY, 6.49);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 2);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "2L");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 132);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7894900011517");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 0); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 0); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 0); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "0ml");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, bebida);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, refrigerante); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, CocaColaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 3259);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7893000631939");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Pizza Mussarela");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Sadia");
		document.append(Constants.PRODUCT_PRICE_KEY, 9.89);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 30);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "440g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 326);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"https://www.magodrive.com.br/Imagem/produtos/7893000631939.png");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 205); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 9.9); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 428); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 12); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "77g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, congelado);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, pizza); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, SadiaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 3594);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7893000632073");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Pizza Calabresa");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Sadia");
		document.append(Constants.PRODUCT_PRICE_KEY, 9.89);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 30);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "460g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 263);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7893000632073");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 189); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 8.7); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 620); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 8.8); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "77g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, congelado);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, pizza); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, SadiaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 2635);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7894904326044");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Pizza Calabresa");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Sadia");
		document.append(Constants.PRODUCT_PRICE_KEY, 9.88);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 30);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "460g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 163);
		document.append(Constants.PRODUCT_URL_IMG_KEY, "https://cdn-cosmos.bluesoft.com.br/products/7893000632073");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 196); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 9.1); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 516); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 8.6); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "77g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, congelado);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, pizza); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, SadiaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 2341);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7894904326044");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Pizza Calabresa");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Seara");
		document.append(Constants.PRODUCT_PRICE_KEY, 9.88);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 30);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "460g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 163);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://santaritasupermercado.com.br/images/produtos//507//20//7894904326044.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 196); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 9.1); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 516); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 8.6); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "77g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, congelado);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, pizza); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, SearaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 2341);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7896376920195");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Linguiça Toscana");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Gold Meat");
		document.append(Constants.PRODUCT_PRICE_KEY, 10.23);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 3);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "600g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 326);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://goldmeat.com.br/storage/app/uploads/public/591/b5b/b50/591b5bb505356544661124.png");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 130); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 11); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 450); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 7.4); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "50g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, carne);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, linguica); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, GolgMeatEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 2657);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "789637691034");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Linguiça de Pernil");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Gold Meat");
		document.append(Constants.PRODUCT_PRICE_KEY, 10.52);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 3);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "470g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 362);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://goldmeat.com.br/storage/app/uploads/public/591/b5b/b50/591b5bb505356544661124.png");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 120); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 10); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 455); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 6.3); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "50g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, carne);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, linguica); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, GolgMeatEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 329);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

	/*	document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "2285000014600");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Iscas de Cox�o Mole");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Top Quality");
		document.append(Constants.PRODUCT_PRICE_KEY, 15.00);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 1);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "400g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 324);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://assets.angeloni.com.br/files/images/8/0C/30/3762524_1_A.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 180); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 11); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 70); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 21); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "100g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, carne);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, iscasCarne); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, TopQualityEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 329);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));*/

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "2118000017015");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Bife de Coxão Mole");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Top Quality");
		document.append(Constants.PRODUCT_PRICE_KEY, 17.00);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 1);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "400g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 362);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://assets.angeloni.com.br/files/images/A/36/F4/3762275_1_A.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 180); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 11); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 70); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 31); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "100g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, carne);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, bife); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, TopQualityEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 231);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "2281900031570");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Maminha");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Marfrig");
		document.append(Constants.PRODUCT_PRICE_KEY, 31.5);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 1);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "1500g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 134);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"http://aogosto.com.br/wp-content/uploads/2016/03/Maminha-Angus-Plena.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 160); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 8); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 60); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 21); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "100g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, carne);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, maminha); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, MarfrigEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 291);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7894904070602");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Lasanha Calabresa");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Seara");
		document.append(Constants.PRODUCT_PRICE_KEY, 9.89);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 3);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "600g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 326);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"https://www.magodrive.com.br/Imagem/produtos/7894904070602.png");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 4200); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 20); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 1162); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 16); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "300g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, congelado);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, lasanha); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, SearaEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 1640);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891515490430");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Lasanha Calabresa");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Perdigão");
		document.append(Constants.PRODUCT_PRICE_KEY, 8.45);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 3);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "600g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 1643);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"https://s3.amazonaws.com/vc-2b/files/produtos/grande/7891515490430-adm-f-1.jpg");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 334); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 17); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 840); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 11); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "300g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, congelado);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, lasanha); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, PerdigaoEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 2630);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		/////////////////////////////////////////////////////////////////////////////////////////////
		//       Margarina com Sal Vigor
		/////////////////////////////////////////////////////////////////////////////////////////////

		document.clear();
		classification.clear();
		nutritionalInfo.clear();
		epcCode.clear();

		document.append(Constants.PRODUCT_BARCODE_KEY, "7891164026639");
		document.append(Constants.PRODUCT_DESCRIPTION_KEY, "Lasanha a Bolonhesa");
		document.append(Constants.PRODUCT_MANUFACTURER_KEY, "Aurora");
		document.append(Constants.PRODUCT_PRICE_KEY, 9.29);
		document.append(Constants.PRODUCT_EXPIRED_TIME_KEY, 3);
		document.append(Constants.PRODUCT_WEIGHT_VOLUME_KEY, "650g");
		document.append(Constants.PRODUCT_QUANTITY_KEY, 132);
		document.append(Constants.PRODUCT_URL_IMG_KEY,
				"https://www.auroraalimentos.com.br/consumidor/thumbs.php?img=../extranet/arquivos/produto/lasanha_a_bolonhesa_aurora_650g_emb_6060.jpg&l=600&a=460");

		nutritionalInfo.append(Constants.PRODUCT_KCAL_KEY, 383); // kcal
		nutritionalInfo.append(Constants.PRODUCT_TOTAL_FAT_KEY, 11); // g
		nutritionalInfo.append(Constants.PRODUCT_SODIUM_KEY, 1095); // mg
		nutritionalInfo.append(Constants.PRODUCT_PROTEIN_KEY, 18); // g
		nutritionalInfo.append(Constants.PRODUCT_REFERENCE_AMMOUNT_KEY, "325g");
		document.append(Constants.PRODUCT_NUTRITIONAL_INFORMATION_KEY, nutritionalInfo);
		document.append(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_PRODUCT_KEY);

		classification.append(Constants.PRODUCT_CLASS_KEY, congelado);
		classification.append(Constants.PRODUCT_ESPECIFICATION_KEY, lasanha); // Margarina
		document.append(Constants.PRODUCT_CLASSIFICATION_KEY, classification);

		epcCode.append(Constants.EPC_MANUFACTURER_KEY, AuroraEPC); // Tirol
		epcCode.append(Constants.EPC_PRODUCT_KEY, 3156);
		document.append(Constants.EPC_KEY, epcCode);

		date = new Date();
		timestamp = new Timestamp(date.getTime());
		document.append(Constants.TIMESTAMP_KEY, timestamp.toString());

		database.updateProductMetaInfo(JSONOperations.documentToJSONObject(document));

		return document.toJson();
	}

	/**
	 * Remove todos os produtos
	 * 
	 * @return
	 */
	public String removeEntries()
	{

		return "OK";
	}
}