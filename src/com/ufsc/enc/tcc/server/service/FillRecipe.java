package com.ufsc.enc.tcc.server.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.util.Constants;
import com.ufsc.enc.tcc.server.util.DataBases;
import com.ufsc.enc.tcc.server.util.JSONOperations;
import com.ufsc.enc.tcc.server.util.MetaInfoConstants;

public class FillRecipe
{
	private MongoDB mongoDB;

	public FillRecipe(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	public void fillEntries()
	{
		//MongoDB mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		JSONObject recipe;
		JSONArray steps;
		JSONArray products;
		JSONObject product;
		JSONObject classification;
		JSONObject prodClass;

		// Receita 1
		try {

			/********************************************************************************************************
			 * 							Tran�a de leite condensado
			 ********************************************************************************************************/
			recipe = new JSONObject();
			steps = new JSONArray();
			products = new JSONArray();
			classification = new JSONObject();

			// Tipo de metainforma��o
			recipe.put(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_RECIPE_KEY);

			// Nome da Receita
			recipe.put(Constants.RECIPE_DESCRIPTION_KEY, "Trança de leite condensado");

			// Passos da receita
			steps.put("Aqueça o forno em temperatura alta, a 200 ºC");
			steps.put("Dissolva o fermento no leite com o açúcar, junte os ovos, o"
					+ " leite condensado e o óleo. Misture bem.");
			steps.put("Aos poucos, adicione a farinha de trigo e trabalhe a massa até ficar homogênea.");
			steps.put("Cubra-a e deixe descansar até dobrar de tamanho.");
			steps.put("Amasse novamente e forme a trança.");
			steps.put("Coloque-a numa assadeira levemente untada e leve ao forno durante 45 minutos ou até dourar.");
			steps.put("Ainda quente, regue com o leite condensado.");

			// Colocar produtos no JSON principal
			recipe.put(Constants.RECIPE_STEPS_KEY, steps);

			recipe.put(Constants.RECIPE_URL_KEY,
					"https://abrilmdemulher.files.wordpress.com/2017/04/receita-torta-marmore-de-leite-condensado163.jpeg?quality=90&strip=info&w=654");
			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Leite Morno"); 			// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.LATICINIO);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.LEITE); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "xícara");
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Fermento"); 			// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "15");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "g");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.CONFEITARIA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.FERMENTO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Açúcar"); 				// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "2");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "colheres de sopa");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.CONFEITARIA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.ACUCAR); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Ovo(s)"); 				// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "4");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.GRANJA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.OVO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Leite Condensado"); 	// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "Lata");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.LATICINIO);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.LEITE_CONDENSADO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "óleo"); 				// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1/2");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "Xícara ch�");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.MERCEARIA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.OLEO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Farinha de trigo"); 	// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "kg");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.FARINHAS);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.FARINHA_TRIGO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);

			products.put(product);

			recipe.put(Constants.PRODUCT_LIST_KEY, products);

			classification.put(Constants.RECIPE_CLASS_KEY, MetaInfoConstants.CANDY_DESSERT);
			classification.put(Constants.RECIPE_ESPECIFICATION_KEY, MetaInfoConstants.MILK_BRAID);

			recipe.put(Constants.RECIPE_CLASSIFICATION_KEY, classification);
			System.out.println(recipe);
			mongoDB.insert(JSONOperations.jsonObjectToDocument(recipe), DataBases.META_INFO_BASE_NAME);

		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		// Receita 2
		try {

			/********************************************************************************************************
			 * 							Doce de leite caseiro
			 ********************************************************************************************************/
			recipe = new JSONObject();
			steps = new JSONArray();
			products = new JSONArray();
			classification = new JSONObject();

			// Tipo de metainforma��o
			recipe.put(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_RECIPE_KEY);

			// Nome da Receita
			recipe.put(Constants.RECIPE_DESCRIPTION_KEY, "Doce de leite caseiro");

			// Passos da receita
			steps.put("Coloque o leite e o açúcar em uma panela grande de fundo largo");
			steps.put(
					"Leve ao fogo médio, mexendo sempre com uma colher de pau, até obter fervura (cerca de 15 minutos)");
			steps.put(
					"Diminua o fogo e continue mexendo até obter um doce marrom claro de consistência cremosa (cerca de 45 minutos)");
			steps.put(
					"PPasse o doce para um refratário, deixe esfriar bem e sirva colheradas em pratos de sobremesa com fatias de queijo branco");

			// Colocar produtos no JSON principal
			recipe.put(Constants.RECIPE_STEPS_KEY, steps);

			recipe.put(Constants.RECIPE_URL_KEY,
					"https://revistasaboresdosul.com.br/wp-content/uploads/2015/08/doce-de-leite-caseiro-640x360.jpg");
			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Leite"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "2");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.LATICINIO);// carne
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.LEITE); // bife
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "L");
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Açúcar"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "4");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "Xícaras");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.CONFEITARIA);// carne
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.ACUCAR); // bife
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			recipe.put(Constants.PRODUCT_LIST_KEY, products);

			classification.put(Constants.RECIPE_CLASS_KEY, MetaInfoConstants.CANDY_DESSERT);
			classification.put(Constants.RECIPE_ESPECIFICATION_KEY, MetaInfoConstants.MILK_CANDY);
			recipe.put(Constants.RECIPE_CLASSIFICATION_KEY, classification);
			System.out.println(recipe);
			mongoDB.insert(JSONOperations.jsonObjectToDocument(recipe), DataBases.META_INFO_BASE_NAME);

		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		// Receita 3
		try {

			/********************************************************************************************************
			 * 							Bife a milanesa
			 ********************************************************************************************************/
			recipe = new JSONObject();
			steps = new JSONArray();
			products = new JSONArray();
			classification = new JSONObject();

			// Tipo de metainforma��o
			recipe.put(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_RECIPE_KEY);

			// Nome da Receita
			recipe.put(Constants.RECIPE_DESCRIPTION_KEY, "Bife a milanesa");

			// Passos da receita
			steps.put("Tempere os bifes a gosto e reserve");
			steps.put("Em um prato fundo, bata os ovos até	 obter uma mistura homogênea");
			steps.put("Separe a farinha de rosca e a farinha de trigo em pratos diferentes");
			steps.put("Passe os bifes na farinha de trigo, depois nos ovos batidos e na farinha de rosca");
			steps.put("Em uma frigideira, frite os bifes em óleo quente até que fiquem dourados");
			steps.put(
					"Ao retirar da frigideira, coloque os bifes em papel toalha para que a gordura em excesso seja absorvida");

			// Colocar produtos no JSON principal
			recipe.put(Constants.RECIPE_STEPS_KEY, steps);

			recipe.put(Constants.RECIPE_URL_KEY,
					"http://www.tudonapanela.com.br/media/k2/items/cache/de2df791682f079f8397226a3ff38bc7_L.jpg");

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Bife"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "0.5");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.CARNE);// carne
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.BIFE); // bife
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "kg");
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Ovos batidos"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "3");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "U");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.GRANJA);// carne
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.OVO); // bife
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Açúcar"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "2");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "Colheres de sopa");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.CONFEITARIA);// carne
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.ACUCAR); // bife
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Farinha de rosca"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "A gosto");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.FARINHAS);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.FARINHA_ROSCA); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Farinha de trigo"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "A gosto");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.FARINHAS);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.FARINHA_TRIGO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Dentes de alho amassados"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "3");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "UN");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.FEIRA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.ALHO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Sal"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "A gosto");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.SAL_PIMENTA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.SAL); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Pimenta do Reino"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "A gosto");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.SAL_PIMENTA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.PIMENTA); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			recipe.put(Constants.PRODUCT_LIST_KEY, products);

			classification.put(Constants.RECIPE_CLASS_KEY, MetaInfoConstants.MEAT);
			classification.put(Constants.RECIPE_ESPECIFICATION_KEY, MetaInfoConstants.ROAST_BEEF);
			recipe.put(Constants.RECIPE_CLASSIFICATION_KEY, classification);

			System.out.println(recipe);
			mongoDB.insert(JSONOperations.jsonObjectToDocument(recipe), DataBases.META_INFO_BASE_NAME);

		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		// Receita 4
		try {

			/********************************************************************************************************
			 * 							P�o Italiano Recheado
			 ********************************************************************************************************/
			recipe = new JSONObject();
			steps = new JSONArray();
			products = new JSONArray();
			classification = new JSONObject();

			// Tipo de metainforma��o
			recipe.put(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_RECIPE_KEY);

			// Nome da Receita
			recipe.put(Constants.RECIPE_DESCRIPTION_KEY, "Pão Italiano Recheado");

			// Passos da receita
			steps.put("Com uma faca de pão, corte fatias paralelas, mas tenha cuidado para não separá-las "
					+ "(deixe elas presas na parte debaixo do pão),depois corte no sentido contrário, formando um xadrez");
			steps.put(
					"Depois regue todo o pão com o azeite. Vá abrindo e regando para que todo o pão fique molhado com o azeite");
			steps.put("Preencha pão com fatias de mussarela, ate que fique bem preenchido");
			steps.put("Polvilhe orégano e leve ao forno pré-aquecido por aproximadamente 10 minutinhos");
			steps.put(
					"Assim que o queijo estiver derretido, retire do forno, enfeite com manjericão (ou outra erva de sua prefer�ncia) e sirva bem quentinho.");

			// Colocar produtos no JSON principal
			recipe.put(Constants.RECIPE_STEPS_KEY, steps);

			recipe.put(Constants.RECIPE_URL_KEY,
					"https://nacozinhadapoly.files.wordpress.com/2013/05/foto-141.jpg?w=640&h=478");

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Pão italiano"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "UN");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.PADARIA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.PAO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Azeite"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "A gosto");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.MERCEARIA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.OLEO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Quejo mussarela"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "A gosto");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.LATICINIO);// carne
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.QUEIJO_MUSSARELA); // bife
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Folhas de Manjericão"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "A gosto");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.TEMPEROS);// carne
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.MANJERICAO); // bife
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			recipe.put(Constants.PRODUCT_LIST_KEY, products);

			classification.put(Constants.RECIPE_CLASS_KEY, MetaInfoConstants.SNACK);
			classification.put(Constants.RECIPE_ESPECIFICATION_KEY, MetaInfoConstants.BREAD);
			recipe.put(Constants.RECIPE_CLASSIFICATION_KEY, classification);
			System.out.println(recipe);
			mongoDB.insert(JSONOperations.jsonObjectToDocument(recipe), DataBases.META_INFO_BASE_NAME);

		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		// Receita 5
		try {

			/********************************************************************************************************
			 * 							Carne de panela
			 ********************************************************************************************************/
			recipe = new JSONObject();
			steps = new JSONArray();
			products = new JSONArray();
			classification = new JSONObject();

			// Tipo de metainforma��o
			recipe.put(Constants.META_INFO_TYPE_KEY, Constants.RECORD_META_RECIPE_KEY);

			// Nome da Receita
			recipe.put(Constants.RECIPE_DESCRIPTION_KEY, "Carne de panela");

			// Passos da receita
			steps.put("Em uma panela de pressão, coloque o óleo junte a cebola, alho e refogue bem");
			steps.put("Acrescente a carne frite por 5 minutos mexendo bem, depois coloque o tempero "
					+ "em p� sabor umami (opcional), tomate, pimentão, massa de tomate, cenoura e a "
					+ "seguir acrescente a água orégano");
			steps.put("Deixe cozinhar por 30 minutos contando o inicio da fervura, assim que a carne "
					+ "estiver cozida retire do fogo, misture a salsinha e sirva em seguida com arroz branco");

			// Colocar produtos no JSON principal
			recipe.put(Constants.RECIPE_STEPS_KEY, steps);

			recipe.put(Constants.RECIPE_URL_KEY,
					"http://www.tudogostoso.com.br/images/recipes/000/086/102/24654/24654_original.jpg");

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Coxão mole cortado em bifes"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "500");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "g");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.CARNE);// carne
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.COXAO_MOLE); // bife
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Cebola ralada"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "UN");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.TEMPEROS);// carne
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.CEBOLA); // bife
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Dente de alho amassado"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.TEMPEROS);// carne
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.ALHO); // bife
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "UN");
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "óleo"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1/2");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "Xícara de chá");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.MERCEARIA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.OLEO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Sal"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "A gosto");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.SAL_PIMENTA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.SAL); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Pimenta-do-reino"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "A gosto");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.SAL_PIMENTA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.PIMENTA); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Tempero em pó sabor umam"); 	// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1/2");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "Colher sopa");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.TEMPEROS);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.UMAM); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Salsinha picada"); 				// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "Colher sopa");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.TEMPEROS);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.SALSINHA); // leite	
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Água quente"); 					// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "500");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "ml");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.AGUA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.AGUA_QUENTE); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Massa de tomate"); 				// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "Meia");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "lata");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.MOLHOS);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.EXTRATO_TOMATE); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Pimentão verde picado"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "UN");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.PIMENTA);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.PIMENTA); // leite	
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Tomate sem sementes picado"); 	// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "UN");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.LEGUMES);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.TOMATE); // leite	
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Cenoura pequena picada"); 		// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "1");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "UN");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.LEGUMES);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.CENOURA); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			product = new JSONObject();
			prodClass = new JSONObject();
			product.put(Constants.PRODUCT_BARCODE_KEY, "Orégano"); 						// Qualquer produto de tal tipo
			product.put(Constants.PRODUCT_QUANTITY_KEY, "A gosto");
			product.put(Constants.PRODUCT_QUANTITY_UNIT_KEY, "");
			prodClass.put(Constants.PRODUCT_CLASS_KEY, MetaInfoConstants.TEMPEROS);// laticínio
			prodClass.put(Constants.PRODUCT_ESPECIFICATION_KEY, MetaInfoConstants.OREGANO); // leite
			product.put(Constants.PRODUCT_CLASSIFICATION_KEY, prodClass);
			products.put(product);

			recipe.put(Constants.PRODUCT_LIST_KEY, products);

			classification.put(Constants.RECIPE_CLASS_KEY, MetaInfoConstants.MEAT);
			classification.put(Constants.RECIPE_ESPECIFICATION_KEY, MetaInfoConstants.PESTLE);
			recipe.put(Constants.RECIPE_CLASSIFICATION_KEY, classification);

			System.out.println(recipe);
			mongoDB.insert(JSONOperations.jsonObjectToDocument(recipe), DataBases.META_INFO_BASE_NAME);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void unfillEntries()
	{

	}
}
