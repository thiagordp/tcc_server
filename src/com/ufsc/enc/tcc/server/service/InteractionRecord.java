/**
 * 
 */
package com.ufsc.enc.tcc.server.service;

import java.sql.Timestamp;
import java.util.Date;

import org.json.JSONObject;

import com.ufsc.enc.tcc.server.database.AuxiliaryStructureBasis;
import com.ufsc.enc.tcc.server.database.InteractionBasis;
import com.ufsc.enc.tcc.server.database.MongoDB;
import com.ufsc.enc.tcc.server.util.Constants;
import com.ufsc.enc.tcc.server.util.GeneralOperations;

/**
 * @author trdp
 *
 */
public class InteractionRecord
{
	private MongoDB mongoDB;

	public InteractionRecord(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	/**
	 * Registra uma nova interação que pode ser relacionado à porta ou a interação com usuário.s
	 */
	public String recordNewInteraction(Integer typeRecord, String data)
	{

		try {
			JSONObject json = new JSONObject(data);								// Criação do obj JSON a partir da string json que contém os EPC codes.		

			if (typeRecord == Constants.TYPE_INTERACTION_RECORD) {				// Caso que interação
				Date date = new Date();
				Timestamp timestamp = new Timestamp(date.getTime());			// Timestamp do servidor
				json.put(Constants.TIMESTAMP_KEY, timestamp.toString());		// Insere no Json	

				InteractionBasis interactionBasis = new InteractionBasis(this.mongoDB);		// Cria instância da base de interação
				interactionBasis.insertInteraction(json);						// Chama o método responsável pela ação.

			}
			else if (typeRecord == Constants.TYPE_INTERACTION_DOOR) {			// Caso seja um registro de mudança do estado da porta

				AuxiliaryStructureBasis structureBasis = new AuxiliaryStructureBasis(this.mongoDB); 	// Instancia a base de estruturas 

				Integer idFridge = json.getInt(Constants.ID_FRIDGE_KEY);					// Extrai a identificação
				Integer status = json.getInt(Constants.DOOR_STATUS_KEY);					// Extrai o estado atual

				structureBasis.setDoorStatus(idFridge, status);								// Grava o estado atual no banco.
			}
			else {
				return GeneralOperations.JSONMessage("Opção de registro de interação inválida!").toString();	// Caso uma opção inválida seja escolhida
			}

			System.out.println(json.toString());

			return Constants.OK_JSON;
		}
		catch (Exception e) {
			System.out.println("Exceção:\t" + e.toString());
			e.printStackTrace();

			return Constants.ERROR_JSON;
		}
	}
}